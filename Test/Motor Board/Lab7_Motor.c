// Lab7_Motor.c
// Runs on TM4C123
// Real Time Operating System for Lab 7's motor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// 2016.05.03

////////////////////////////////////////////////////////////////////////////////
// Timers

//  SysTick - OS_Launch() ; priority = 7
//  Timer4A - OS_AddPeriodicThread(), Os_AddAperiodicThread() ; priority = 1
//  Timer5A - OS_Init(), OS_Time(), O  S_TimeDifference(), OS_ClearMsTime(),
//            OS_MsTime() ; priority = 0

////////////////////////////////////////////////////////////////////////////////
// OS Background Threads

//  OS_Aging - priority = 7 (added if PRIORITY_SCHEDULER is defined in OS.h)
//  OS_Sleep - priority = 1

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "ADC.h"
#include "Buttons.h"
#include "CAN_4C123\can0.h"
#include "Controller.h"
#include "Interrupts.h"
#include "IR.h"
#include "Lab7_Motor.h"
#include "Motors.h"
#include "OS.h"
#include "Profiler.h"
#include "Retarget.h"
#include "SysTick.h"
#include "Timer5.h"
#include "tm4c123gh6pm.h"
#ifdef DEBUG
    #include "UART.h"
    #include "Interpreter.h"
#endif

////////////////////////////////////////////////////////////////////////////////
// Global Variables

uint8_t FirstRun;         // indicates whether 3min race has started
uint32_t IdleCount;       // counts iterations of idleTask()
uint32_t IR0_Dist;        // IR0 data sent from sensor board
uint32_t IR1_Dist;        // IR1 data sent from sensor board
uint32_t IR2_Dist;        // IR2 data sent from sensor board
uint32_t IR3_Dist;        // IR3 data sent from sensor board
uint32_t StartRace;       // Start race signal sent from sensor board

////////////////////////////////////////////////////////////////////////////////
// idleTask()
// Foreground thread. 
// Runs when no other work is needed.
// Never blocks, sleeps, or dies.
// inputs:  none
// outputs: none

void IdleTask(void){ 
  while(1) { 
        PF1 ^= 0x02;        // debugging profiler 
        IdleCount++;        // debugging 
        WaitForInterrupt(); // low-power mode
  }
}

////////////////////////////////////////////////////////////////////////////////
// Interpreter()
// Foreground thread.
// Accepts input from serial port, outputs to serial port.
// inputs:  none
// outputs: none

#ifdef DEBUG
    void Interpreter(void) {
        Interpreter_Init();
        while(1){
            Interpreter_Parse();
        }
    }
#endif

////////////////////////////////////////////////////////////////////////////////
// CANConsumer()
// Foreground thread.
// Retrieves sensor data from CAN FIFO.
// inputs:  none
// outputs: none
    
void CANConsumer(void){ 
  while(1){
        if(CAN0PutPt!=CAN0GetPt) {  // if not empty, then get from FIFO
            do{                                           
                // retrieve all CAN messages in FIFO
                IR0_Dist = CAN0_Fifo_Get();
                IR1_Dist = CAN0_Fifo_Get();
                IR2_Dist = CAN0_Fifo_Get();
                IR3_Dist = CAN0_Fifo_Get();
                CAN0_Fifo_Get();
                CAN0_Fifo_Get();
                CAN0_Fifo_Get();
                StartRace = CAN0_Fifo_Get();
                
                #ifdef DEBUG
                    // print results
                    printf("IR0(cm)= %u\n\r", IR0_Data);
                    printf("IR1(cm)= %u\n\r", IR1_Data);
                    printf("IR2(cm)= %u\n\r", IR2_Data);
                    printf("IR3(cm)= %u\n\r", IR3_Data);
                    printf("Bumper0 = %u\n\r", Bumper0_Data);
                    printf("Bumper1 = %u\n\r", Bumper1_Data);
                    printf("StartRace = %u\n\r", StartRace);
                #endif
                        
                // Start Race
                if(FirstRun == 0 && StartRace){
                    FirstRun = 1;
                                        
                    // add threads needed for race
                    OS_AddButtonTask(&Bumper0Task, PC6_TASK, 3);
                    OS_AddButtonTask(&Bumper1Task, PC7_TASK, 3);
                    OS_AddPeriodicThread(&simpleController, FIFTY_HZ, 1);
                    Timer5A_Enable();   // Stop robot after 3 minutes

                    break;
                }
            }while(CAN0PutPt!=CAN0GetPt);
        }
    }   // runs forever, does not get killed
}  

int main(void){  
    // intialize globals
    IdleCount = 0;
    FirstRun = 0;
    RunController = true;
         
    // initalize modules  
    OS_Init();      // initialize OS, disable interrupts
    PortF_Init();   // initialize Port F profiling
    CAN0_Open();    // initialize CAN0
    IR_Init_HW();   // initialize IR sensors 4-7
    motorInit();    // initialize drive PWMs, drive motors, and servos
    
    // create initial foreground threads
    OS_AddThread(&IdleTask, 128, 7);
    OS_AddThread(&CANConsumer, 128, 1);
    #ifdef DEBUG
        OS_AddThread(&Interpreter, 128, 4);
    #endif

    OS_Launch(TIMESLICE);   // doesn't return, interrupts enabled in here
    return 0;               // this never executes
}
