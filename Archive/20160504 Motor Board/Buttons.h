// Buttons.h
// Runs on TM4C123
// Request an interrupt on the falling edge of PF0 and/or PF4 (when the 
// user buttons are pressed) Note that button bouncing is not addressed.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// 2016.05.03

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdlib.h>
#include <stdint.h>
#include "Config.h"

////////////////////////////////////////////////////////////////////////////////
// System Settings

#define SW1       0x10                      // on the left side of the Launchpad board
#define SW2       0x01                      // on the right side of the Launchpad board

////////////////////////////////////////////////////////////////////////////////
// Global Variables

extern volatile uint8_t Bumper0_Data;    // Bumper0 data
extern volatile uint8_t Bumper1_Data;    // Bumper1 data

// ******** SW1_Init ************
// Initialize Port F4 to handle edge triggered interrupt when SW1 is pressed. 
// Store SW1's task and priority.
// input:  *task - pointer to SW1's user task
//         priority - SW1's task priority
// output: none
void SW1_Init(void(*task)(void), uint8_t priority);

// ******** SW2_Init ************
// Initialize Port F0 to handle edge triggered interrupt when SW2 is pressed. 
// Store SW2's task and priority.
// input:  *task - pointer to SW2's user task
//         priority - SW1's task priority
// output: none
void SW2_Init(void(*task)(void), uint8_t priority);

// ******** BUMPER0_Init ************
// Initialize PC6 to handle edge triggered interrupt with BUMPER0. 
// Store BUMPER0's task and priority.
// input:  *task - pointer to BUMPER0's user task
//         priority - BUMPER0's task priority
// output: none
void BUMPER0_Init(void(*task)(void), uint8_t priority);

// ******** BUMPER1_Init ************
// Initialize PC7 to handle edge triggered interrupt with BUMPER1. 
// Store BUMPER1's task and priority.
// input:  *task - pointer to BUMPER1's user task
//         priority - BUMPER1's task priority
// output: none
void BUMPER1_Init(void(*task)(void), uint8_t priority);

// ***************** Switch_Enable ******************
void Switch_Enable(void);

// ***************** Switch_Disable ******************
void Switch_Disable(void);


//******** Bumper0Task **************
// Aperiodic background thread.  
// Called when bumper0 is pressed, and sets state of
// robot into reverse left.
// inputs:  none
// outputs: none
void Bumper0Task(void);

//******** Bumper1Task **************
// Aperiodic background thread.  
// Called when bumper1 is pressed, and sets state of
// robot into reverse right.
// inputs:  none
// outputs: none
void Bumper1Task(void);
