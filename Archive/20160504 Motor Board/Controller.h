// Controller.h
// Runs on TM4C123
// Feedback controller for robot's motor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// 2016.05.03

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include "Config.h"
#include <stdint.h>

////////////////////////////////////////////////////////////////////////////////
// Global Variables

extern uint8_t RunController;  // determines if the simple controller should run

////////////////////////////////////////////////////////////////////////////////
// simpleController()
// Runs simple PID controllers to balance pairs of symmetrical IR sensors.
// Input: new sensor data
// Output: motor speed and servo position commands

void simpleController(void);
