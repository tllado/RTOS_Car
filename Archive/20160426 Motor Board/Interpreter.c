// Interpreter.c
// Runs on TM4C123 
// Implements an interpreter using the UART serial port and interrupting I/O. 
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// March 20th, 2016

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "Interpreter.h"
#include "UART.h"
#include "Lab7_Motor.h"
#include "Interrupts.h"
#include "PWM.h"
#include "OS.h"
#include "CAN_4C123\can0.h"


// *************Global Variables*****************************

cmdTable Commands[] = {        // CMD_LEN defines max command name length
	{"help",     cmdHelp,        "Displays all available commands."},
  {"motor",    cmdMotor,       "Sends commands to the motors."},
	{"servo",    cmdServo,       "sends commands to the servo."},
	{"can",      cmdCAN,         "Control CAN"},
};

char cmd[CMD_LEN+1];   // string to store command line inputs. +1 for null.
// *********************************************************************


void Interpreter_Init(void){
	printf("\n\n\r******************************************************\n\r");
	printf("                  Welcome to bOS.\n\r");
	printf("          Running software for motor board\n\r");
	printf("         Type \"help\" for a list of commands.\n\r");
	printf("******************************************************\n\r");
}


//---------Interpreter_Parse-------
// Compare user's input with table of 
// available commands.
void Interpreter_Parse(void){
	printf("\n\r--Enter Command--> ");
	UART_InStringNL(cmd, CMD_LEN);  
  int i = NUM_OF(Commands);
  while(i--){
    if(!strcmp(cmd,Commands[i].name)){  // check for valid command
      Commands[i].func();
      return;
    }
  }
	printf("Command not recognized.\n\r");
}

	
//------------cmdHelp-----------------
// Display all the available commands
void cmdHelp(void){
	printf("Below is a list of availble commands:\n\r");
	// output formating and display command name
	for(int i = 0; i < NUM_OF(Commands); i++){
		if(i+1 <10) printf("    ");                   
		else if(i+1 < 100) printf("   ");     
		printf("%d",i+1);                             
		printf(") ");                         
		// display command name
		printf("%s",(char*)Commands[i].name);  
    // output formating		
		for(int j = strlen(Commands[i].name); j<CMD_LEN ; j++){
		  printf("-");                             
		}
		// display command tag
    printf("%s\n\r",(char*)Commands[i].tag);     
	}
}

//------------cmdMotor-----------------
// Sends commands to the motor
void cmdMotor(void){
	printf("Choose a command for the motors:\n\r");
	printf("    **************** Motor 1 ****************\n\r");
	printf("    (1) Duty - Assign duty cycle of motor 1\n\r");
	printf("    (2) Forward - Motor rotates forward of motor 1\n\r");
	printf("    (3) Reverse - Motor rotates in reverse of motor 1\n\r");
	printf("    (4) Coast - Motor stops (fast decay) of motor 1\n\r");
	printf("    (5) Brake - Motor stops (slow decay) of motor 1\n\n\r");
	printf("    **************** Motor 2 ****************\n\r");
	printf("    (6) Duty - Assign duty cycle of motor 2\n\r");
	printf("    (7) Forward - Motor rotates forward of motor 2\n\r");
	printf("    (8) Reverse - Motor rotates in reverse of motor 2\n\r");
	printf("    (9) Coast - Motor stops (fast decay) of motor 2\n\r");
	printf("    (10) Brake - Motor stops (slow decay) of motor 2\n\r");
	printf("--Enter Command #--> ");
	uint32_t num = UART_InUDec();
	printf("\n\r");
	
	switch (num){
		// Duty
	  case(1):{
			printf("Enter the new duty cycle (in %%): ");
			uint16_t percent = UART_InUDec();
			printf("\n\n\r");
			uint32_t duty = (PWM_MOTOR_FREQ*percent)/100;
			
			if((duty > PWM_MOTOR_FREQ) || (duty > (1<<16))){
				printf("Error, duty cycle not compatable\n\r");
			}
			else{
				int32_t status = StartCritical();
				DutyPWM0 = duty;
			  EndCritical(status);
				printf("Duty cycle assigned\n\r");
			}
			break;
		}
		
		// Forward
		case(2):{
			PWM0A_Duty(DutyPWM0);
			PWM0B_Duty(0);
			printf("Motor moving forward.\n\r");
			break;
		}
		
		// Reverse
		case(3):{
			PWM0A_Duty(0);
			PWM0B_Duty(DutyPWM0);
			printf("Motor moving in reverse.\n\r");
			break;
		}
		
		// Coast
		case(4):{
			PWM0A_Duty(0);
			PWM0B_Duty(0);
			printf("Motor is coasting.\n\r");
			break;
		}
		
		// Brake
		case(5):{
			PWM0A_Duty(DutyPWM0);
			PWM0B_Duty(DutyPWM0);
			printf("Motor is breaking.\n\r");
			break;
		}
		
		// Duty
	  case(6):{
			printf("Enter the new duty cycle (in %%): ");
			uint16_t percent = UART_InUDec();
			printf("\n\n\r");
			uint32_t duty = (PWM_MOTOR_FREQ*percent)/100;
			
			if((duty > PWM_MOTOR_FREQ) || (duty > (1<<16))){
				printf("Error, duty cycle not compatable\n\r");
			}
			else{
				int32_t status = StartCritical();
				DutyPWM01 = duty;
			  EndCritical(status);
				printf("Duty cycle assigned\n\r");
			}
			break;
		}
		
		// Forward
		case(7):{
			PWM01A_Duty(0);
			PWM01B_Duty(DutyPWM01);
			printf("Motor moving in reverse.\n\r");
			break;
		}
		
		// Reverse
		case(8):{
			PWM01A_Duty(DutyPWM01);
			PWM01B_Duty(0);
			printf("Motor moving forward.\n\r");
			break;
		}
		
		// Coast
		case(9):{
			PWM01A_Duty(0);
			PWM01B_Duty(0);
			printf("Motor is coasting.\n\r");
			break;
		}
		
		// Brake
		case(10):{
			PWM01A_Duty(DutyPWM01);
			PWM01B_Duty(DutyPWM01);
			printf("Motor is breaking.\n\r");
			break;
		}
		// Improper Selection
		default: {
			printf("Improper selection\n\r"); 
			return;
		}
	}		
}


//------------cmdServo-----------------
// Control the servo for steering. 0 degrees is far right, 
// 30 degrees is center and 60 degrees is far left.
void cmdServo(void){
	printf("Choose a command for the servo:\n\r");
	printf("    (1) Angle - Assign desired angle to servo\n\r");
	printf("    (2) Controller - Assign inputs for servo's PID controller\n\r");
	printf("--Enter Command #--> ");
	uint32_t selection = UART_InUDec();
	printf("\n\r");
	
  switch(selection){
		// View data
		 case(1):{
			 printf("How many degrees do you want to turn the servo? (0=right, 30=center, 60=left): ");
	     uint32_t angle = UART_InUDec();
	     if(angle>60){
		     printf("\n\rImproper selection\n\r"); 
		     return;
	     }
	     else{  
		     PWM1A_Duty(SERVO(angle));                     //using servo equation
				 printf("\n\rThe angle has been set.\n\r");
	     }
       break;
		 }
        
		 case(2):{

        break;
			}
		 
	  default: {
      printf("Improper selection\n\r"); 
      return;
    }
	}
}


//------------cmdCAN-----------------
// Control CAN.
void cmdCAN(void){
	printf("Choose a command for CAN:\n\r");
	printf("    (1) Get Data - Get the data received in FIFO.\n\r");
	printf("    (2) Send Data - Send four bytes of data.\n\r");
	printf("    (3) Lost Packets - Display how many packets were lost.\n\r");

	printf("--Enter Command #--> ");
	uint32_t selection = UART_InUDec();
	printf("\n\r");
	
	switch(selection){
		// View data
		 case(1):{
			  uint8_t data[8];
			  unsigned long counter = 1;
			  printf("The data in the CAN0 fifo is as follows:\n\r");
			  if(CAN0PutPt!=CAN0GetPt){//if there is one
					do{//for all data in fifo
					  data[0] = CAN0_Fifo_Get();//Byte 0
			      data[1] = CAN0_Fifo_Get();//Byte 1
			      data[2] = CAN0_Fifo_Get();//Byte 2
			      data[3] = CAN0_Fifo_Get();//Byte 3
						data[4] = CAN0_Fifo_Get();//Byte 4
			      data[5] = CAN0_Fifo_Get();//Byte 5
			      data[6] = CAN0_Fifo_Get();//Byte 6
			      data[7] = CAN0_Fifo_Get();//Byte 7
						//get data and print it
			      printf("Output %lu PING0 = %u\n\r", counter, data[0]);
						printf("Output %lu PING1 = %u\n\r", counter, data[1]);
						printf("Output %lu PING2 = %u\n\r", counter, data[2]);
						printf("Output %lu PING3 = %u\n\r", counter, data[3]);
						printf("Output %lu IR0 = %u\n\r", counter, data[4]);
						printf("Output %lu IR1 = %u\n\r", counter, data[5]);
						printf("Output %lu IR2 = %u\n\r", counter, data[6]);
						printf("Output %lu IR3 = %u\n\r\n\r", counter, data[7]);
						counter++;//increment counter
					}while(CAN0PutPt!=CAN0GetPt);
				}
				else{//if there is none
					printf("none");
					printf("\n\r");
				}
        break;
    }
	 // Send data
		case(2):{
			uint8_t data[8];
			uint32_t input;
			for(int i = 0; i < 8; i++){//for Bytes 0-7
				switch(i){//for every different sensor
					case 0:
			      printf("What is the input from PING0 (0-255): ");
					  break;
					case 1:
			      printf("\n\rWhat is the input from PING1 (0-255): ");
					  break;
					case 2:
			      printf("\n\rWhat is the input from PING2 (0-255): ");
					  break;
					case 3:
			      printf("\n\rWhat is the input from PING3 (0-255): ");
					  break;
					case 4:
			      printf("\n\rWhat is the input from IR0 (0-255): ");
					  break;
					case 5:
			      printf("\n\rWhat is the input from IR1 (0-255): ");
					  break;
					case 6:
			      printf("\n\rWhat is the input from IR2 (0-255): ");
					  break;
					case 7:
			      printf("\n\rWhat is the input from IR3 (0-255): ");
					  break;
				}
			  input=UART_InUDec();
			  if(input>255){
				  printf("\n\rImproper selection\n\r"); 
			    return;
			  }
			  else{
				  data[i] = input&0x000000FF;//store the byte
			  }
		  }
			CAN0_SendData(data);//send 8 bytes of data
			printf("\n\rYour inputs were sent.\n\r");
      break;
    }
		// Lost packets
    case(3):{
      printf("\n\r%lu packets have been lost.\n\r", PacketLost);
      break;
    }
    default: {
      printf("Improper selection\n\r"); 
      return;
    }
	}
}

