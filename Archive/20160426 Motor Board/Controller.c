// Controller.c
// Runs on TM4C123
// Feedback controller for robot's motor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 20th, 2016

#include <stdint.h>
#include <stdbool.h>
#include "Controller.h"
#include "Lab7_Motor.h"
#include "PWM.h"
#include "Profiler.h"

//******************** Globals ***********************************************
StateType State_Servo;              // state machine for servo
StateType SavedState_Servo;         // saves the state of the servo when a collision occurs
// ***************************************************************************

// ******** Motor_Controller ************
// Adjust the duty cycles for both motors
// Inputs: dutyPercentA - Percent of duty you wish to apply to motor A (0-100)
//         directionA - Direction you wish to spin motor A
//         dutyPercentB - Percent of duty you wish to apply to motor B (0-100)
//         directionB - Direction you wish to spin motor B
// Output: none
void Motor_Controller (uint8_t dutyPercentA, DirType directionA, uint8_t dutyPercentB, DirType directionB){
	uint16_t dutyA = (PWM_MOTOR_FREQ*dutyPercentA)/100;
	uint16_t dutyB = (PWM_MOTOR_FREQ*dutyPercentB)/100;
	switch(directionA){
		// move forward
		case DIR_FORWARD:{
	    PWM0A_Duty(dutyA);    // motor A
			PWM0B_Duty(0);        // motor A
			break;
		}
		
		// move in reverse
		case DIR_REVERSE:{
		  PWM0A_Duty(0);        // motor A
			PWM0B_Duty(dutyA);    // motor A
			break;
		}
	}
	
	switch(directionB){
		case DIR_FORWARD:{			
	    PWM01B_Duty(dutyB);   // motor B
	    PWM01A_Duty(0);       // motor B
			break;
		}
		
		// move in reverse
		case DIR_REVERSE:{			
	    PWM01B_Duty(0);       // motor B
	    PWM01A_Duty(dutyB);   // motor B
			break;
		}
	}
}


//******** Servo_Controller **************
// Calculates the output that should be applied to servo based on 
// measured, desired, and kp values.
// inputs:  none
// outputs: none
void Servo_Controller(int32_t measured, int32_t desired, int32_t kp){
	// calculate error
	int32_t e = measured - desired; 
	
	// calculate PID terms
  int32_t p = kp * e;             
	int32_t u = p + SERVO_CENTER; 
	
	// constrain outputs
  if(u > SERVO_LEFT){
  	u = SERVO_LEFT;
  }
  if(u < SERVO_RIGHT){
  	u = SERVO_RIGHT;
  }
			
	// apply new output to servo
	PWM1A_Duty(u);
}


//******** Servo_StateMachine_CW **************
// Perioidic background thread.
// Controls servo using PID assuming the robot moves around 
// the track in a clockwise manner.
// inputs:  none
// outputs: none
void Servo_StateMachine_CW(void){
  switch(State_Servo){
		///////////////////////////////////
		//       STATE = STRAIGHT        //
		///////////////////////////////////
		case ST_STRAIGHT: {
			// check if robot has bumper0 action on right and needs to reverse
			if(Check_Reverse_Right()){
				SavedState_Servo = ST_STRAIGHT;
				State_Servo = ST_REVERSE_RIGHT;
				break;
			}
			
			// check if robot has bumper1 action on right and needs to reverse
			if(Check_Reverse_Left()){
				SavedState_Servo = ST_STRAIGHT;
				State_Servo = ST_REVERSE_LEFT;
				break;
			}
			
			// check if robot has obstacle on right and need to turn left
			if(Check_Obstacle_Right()){
				State_Servo = ST_OBSTACLE_RIGHT;
				break;
			}
			
			// check if robot has obstacle on left and need to turn right
			if(Check_Obstacle_Left()){
				State_Servo = ST_OBSTACLE_LEFT;
				break;
			}
			
			// check if robot needs to turn right
		  if(Check_Turn_Right()){
				State_Servo = ST_TURN_RIGHT;
				break;
			}
			
			// check if robot needs to turn left
			if(Check_Turn_Left()){
        State_Servo = ST_TURN_LEFT;
				break;
			}
			
			// check if robot needs to slide right
			if(Check_Slide_Right()){
				State_Servo = ST_SLIDE_RIGHT;
				break;
			}
			
			// check if robot needs to slide left
			if(Check_Slide_Left()){
				State_Servo = ST_SLIDE_LEFT;
				break;
			}
			
			// else, set duty cycle and move straight
			Motor_Controller(STRAIGHT_DUTY, DIR_FORWARD, STRAIGHT_DUTY, DIR_FORWARD);
		  PWM1A_Duty(SERVO_CENTER);
	    break;
		}
		
		///////////////////////////////////
		//       STATE = TURN_RIGHT      //
		///////////////////////////////////	
		case ST_TURN_RIGHT: {
			// check if robot has bumper0 action on right and needs to reverse
			if(Check_Reverse_Right()){
				SavedState_Servo = ST_TURN_RIGHT;
				State_Servo = ST_REVERSE_RIGHT;
				break;
			}
			
			// check if robot has bumper1 action on right and needs to reverse
			if(Check_Reverse_Left()){
				SavedState_Servo = ST_TURN_RIGHT;
				State_Servo = ST_REVERSE_LEFT;
				break;
			}
			
			// check if robot has obstacle on right and need to turn left
			if(Check_Obstacle_Right()){
				State_Servo = ST_OBSTACLE_RIGHT;
				break;
			}
			
			// check if robot has obstacle on left and need to turn right
			if(Check_Obstacle_Left()){
				State_Servo = ST_OBSTACLE_LEFT;
				break;
			}
			
			// check if robot needs to move straight
			if(Check_Straight()){
				State_Servo = ST_STRAIGHT;
				break;
			}
			
			// check if robot needs to turn left
			if(Check_Turn_Left()){
        State_Servo = ST_TURN_LEFT;
				break;
			}
			
			// set motors duty cycle and calculate value to output to servoe
			Motor_Controller(TURN_RIGHT_DUTY, DIR_FORWARD, TURN_RIGHT_DUTY, DIR_FORWARD);
			Servo_Controller(IR2_Data, IR2_DIST_MAX, KP_TURN_R_SERVO);
		  break;
		}
		
		///////////////////////////////////
		//       STATE = TURN_LEFT       //
		///////////////////////////////////	
		case ST_TURN_LEFT: {
			// check if robot has bumper0 action on right and needs to reverse
			if(Check_Reverse_Right()){
				SavedState_Servo = ST_TURN_LEFT;
				State_Servo = ST_REVERSE_RIGHT;
				break;
			}
			
			// check if robot has bumper1 action on right and needs to reverse
			if(Check_Reverse_Left()){
				SavedState_Servo = ST_TURN_LEFT;		
				State_Servo = ST_REVERSE_LEFT;
				break;
			}
			
			// check if robot has obstacle on right and need to turn left
			if(Check_Obstacle_Right()){
				State_Servo = ST_OBSTACLE_RIGHT;
				break;
			}
			
			// check if robot has obstacle on left and need to turn right
			if(Check_Obstacle_Left()){
				State_Servo = ST_OBSTACLE_LEFT;
				break;
			}
			
			// check if robot needs to move straight
			if(Check_Straight()){
				State_Servo = ST_STRAIGHT;
				break;
			}
			
			// check if robot needs to turn right
      if(Check_Turn_Right()){
				State_Servo = ST_TURN_RIGHT;
				break;
			}
			
			// set motors duty cycle and calculate value to output to servo
			Motor_Controller(TURN_LEFT_DUTY, DIR_FORWARD, TURN_LEFT_DUTY, DIR_FORWARD);
			Servo_Controller(IR1_Data, IR1_DIST_MAX, KP_TURN_L_SERVO);
			break;
		}
		
		///////////////////////////////////
		//      STATE = SLIDE_RIGHT      //
		///////////////////////////////////	
		case ST_SLIDE_RIGHT: {
			// check if robot has bumper0 action on right and needs to reverse
			if(Check_Reverse_Right()){
				SavedState_Servo = ST_SLIDE_RIGHT;
				State_Servo = ST_REVERSE_RIGHT;
				break;
			}
			
			// check if robot has bumper1 action on right and needs to reverse
			if(Check_Reverse_Left()){
				SavedState_Servo = ST_SLIDE_RIGHT;				
				State_Servo = ST_REVERSE_LEFT;
				break;
			}
			
			// check if robot has obstacle on right and need to turn left
			if(Check_Obstacle_Right()){
				State_Servo = ST_OBSTACLE_RIGHT;
				break;
			}
			
			// check if robot has obstacle on left and need to turn right
			if(Check_Obstacle_Left()){
				State_Servo = ST_OBSTACLE_LEFT;
				break;
			}
			
			// check if robot needs to turn right
      if(Check_Turn_Right()){
				State_Servo = ST_TURN_RIGHT;
				break;
			}
			
			// check if robot needs to turn left
			if(Check_Turn_Left()){
        State_Servo = ST_TURN_LEFT;
				break;
			}
			
			// check if robot needs to go straight from slide
			if(Check_Straight()){
        State_Servo = ST_STRAIGHT;
				break;
			}
			
			// check if robot needs to slide left
			if(Check_Slide_Left()){
				State_Servo = ST_SLIDE_LEFT;
				break;
			}
			
			// set motors duty cycle and calculate value to output to servo
			Motor_Controller(SLIDE_RIGHT_DUTY, DIR_FORWARD, SLIDE_RIGHT_DUTY, DIR_FORWARD);
			Servo_Controller(IR3_Data, IR3_DIST_MAX, KP_SLIDE_R_SERVO);
			break;
		}
		
    ///////////////////////////////////
		//      STATE = SLIDE_LEFT       //
		///////////////////////////////////	
		case ST_SLIDE_LEFT: {
			// check if robot has bumper0 action on right and needs to reverse
			if(Check_Reverse_Right()){
				SavedState_Servo = ST_SLIDE_LEFT;
				State_Servo = ST_REVERSE_RIGHT;
				break;
			}
			
			// check if robot has bumper1 action on right and needs to reverse
			if(Check_Reverse_Left()){
			  SavedState_Servo = ST_SLIDE_LEFT;
				State_Servo = ST_REVERSE_LEFT;
				break;
			}
			
			// check if robot has obstacle on right and need to turn left
			if(Check_Obstacle_Right()){
				State_Servo = ST_OBSTACLE_RIGHT;
				break;
			}
			
			// check if robot has obstacle on left and need to turn right
			if(Check_Obstacle_Left()){
				State_Servo = ST_OBSTACLE_LEFT;
				break;
			}
			
			// check if robot needs to turn right
      if(Check_Turn_Right()){
				State_Servo = ST_TURN_RIGHT;
				break;
			}
			
			// check if robot needs to turn left
			if(Check_Turn_Left()){
        State_Servo = ST_TURN_LEFT;
				break;
			}
			
			// check if robot needs to go straight
			if(Check_Straight()){
        State_Servo = ST_STRAIGHT;
				break;
			}
			
			// check if robot needs to slide right
			if(Check_Slide_Right()){
				State_Servo = ST_SLIDE_RIGHT;
				break;
			}
			
			// set motors duty cycle and calculate value to output to servo
			Motor_Controller(SLIDE_LEFT_DUTY, DIR_FORWARD, SLIDE_LEFT_DUTY, DIR_FORWARD);
			Servo_Controller(IR0_Data, IR0_DIST_MAX, KP_SLIDE_L_SERVO);
		  break;
		}
		
		///////////////////////////////////
		//    STATE = OBSTACLE_RIGHT     //
		///////////////////////////////////	
		case ST_OBSTACLE_RIGHT: {
			// check if robot has bumper0 action on right and needs to reverse
			if(Check_Reverse_Right()){
				SavedState_Servo = ST_OBSTACLE_RIGHT;				
				State_Servo = ST_REVERSE_RIGHT;
				break;
			}
			
			// check if robot has bumper1 action on right and needs to reverse
			if(Check_Reverse_Left()){
				SavedState_Servo = ST_OBSTACLE_RIGHT;						
				State_Servo = ST_REVERSE_LEFT;
				break;
			}
			
			if(Check_Straight()){
        State_Servo = ST_STRAIGHT;
				break;
			}
			
			// set motors duty cycle and calculate value to output to servo
			Motor_Controller(0, DIR_FORWARD, OBSTACLE_RIGHT_DUTY, DIR_FORWARD);
			Servo_Controller(IR2_Data, IR2_DIST_MIN, KP_OBSTACLE_R_SERVO);
			break;	
		}
		
	  ///////////////////////////////////
		//    STATE = OBSTACLE_LEFT     //
		///////////////////////////////////	
		case ST_OBSTACLE_LEFT: {
			// check if robot has bumper0 action on right and needs to reverse
			if(Check_Reverse_Right()){
				SavedState_Servo = ST_OBSTACLE_LEFT;						
				State_Servo = ST_REVERSE_RIGHT;
				break;
			}
			
			// check if robot has bumper1 action on right and needs to reverse
			if(Check_Reverse_Left()){
				SavedState_Servo = ST_OBSTACLE_LEFT;										
				State_Servo = ST_REVERSE_LEFT;
				break;
			}
			
			if(Check_Straight()){
        State_Servo = ST_STRAIGHT;
				break;
			}
			
			// set motors duty cycle and calculate value to output to servo
			Motor_Controller(OBSTACLE_LEFT_DUTY, DIR_FORWARD, 0, DIR_FORWARD);
			Servo_Controller(IR1_Data, IR1_DIST_MIN, KP_OBSTACLE_L_SERVO);
			break;
		}
	
		///////////////////////////////////
		//    STATE = REVERSE_RIGHT     //
		///////////////////////////////////	
		case ST_REVERSE_RIGHT: {
			if(Check_Reverse_Right_Clear()){
				State_Servo = SavedState_Servo;
				break;
			}
			Motor_Controller(REVERSE_DUTY, DIR_REVERSE, REVERSE_DUTY, DIR_REVERSE);
		  PWM1A_Duty(SERVO_RIGHT);
			
	    break;
		}
		
	  ///////////////////////////////////
		//    STATE = REVERSE_LEFT     //
		///////////////////////////////////	
		case ST_REVERSE_LEFT: {
			if(Check_Reverse_Left_Clear()){
				State_Servo = SavedState_Servo;
				break;
			}
			Motor_Controller(REVERSE_DUTY, DIR_REVERSE, REVERSE_DUTY, DIR_REVERSE);
		  PWM1A_Duty(SERVO_LEFT);
	    break;
		}
	}
}


//******** Check_Straight **************
// Checks if robot needs to enter go straight state
// inputs:  none
// outputs: true - needs to enter go straight state, 
//          false - stay in same state
uint8_t Check_Straight(void){
	// if in obstacle right state, only go straight once cleared of obstacle
	if(State_Servo == ST_OBSTACLE_RIGHT){
	  if(IR2_Data >= IR2_DIST_MIN){
			return true;
		}
		else{
			return false;
		}
	}
	
	// if in obstacle left state, only go straight once cleared of obstacle
	if(State_Servo == ST_OBSTACLE_LEFT){
	  if(IR1_Data >= IR1_DIST_MIN){
			return true;
		}
		else{
			return false;
		}
	}
	
	// if in a slide state, already have a straight path
	if((State_Servo == ST_SLIDE_RIGHT) || (State_Servo == ST_SLIDE_LEFT)){
		if((IR0_Data <= IR0_DIST_MAX) && (IR3_Data <= IR3_DIST_MAX)){
			return true;
		}
	  else{
	  	return false;
	  }	
	}
	
	// check used if not in a obstacle or slide states
	if(Ping1_Data > PING1_STRAIGHT){
	  return true;
	}
	else{
	  return false;
	}	
}


//******** Check_Turn_Right **************
// Checks if robot needs to enter turn right state
// inputs:  none
// outputs: true - needs to enter turn right state, 
//          false - stay in same state
uint8_t Check_Turn_Right(void){
	if((IR2_Data > IR2_DIST_MAX) && (Ping1_Data <= PING1_STRAIGHT)){
		return true;
	}
	else{
		return false;
	}
}


//******** Check_Turn_Left **************
// Checks if robot needs to enter turn left state
// inputs:  none
// outputs: true - needs to enter turn left state, 
//          false - stay in same state
uint8_t Check_Turn_Left(void){
	if((IR1_Data > IR1_DIST_MAX) && (Ping1_Data <= PING1_STRAIGHT)){
		return true;
	}
	else{
		return false;
	}
}


//******** Check_Slide_Right **************
// Checks if robot needs to enter slide right state
// inputs:  none
// outputs: true - needs to enter slide right state, 
//          false - stay in same state
uint8_t Check_Slide_Right(void){
	if(IR3_Data >= IR3_DIST_MAX){
		return true;
	}
	else{
		return false;
	}
}


//******** Check_Slide_Left **************
// Checks if robot needs to enter slide left state
// inputs:  none
// outputs: true - needs to enter slide left state, 
//          false - stay in same state
uint8_t Check_Slide_Left(void){
  if(IR0_Data >= IR0_DIST_MAX){
		return true;
	}
	else{
		return false;
	}
}


//******** Check_Obstacle_Right **************
// Checks if robot needs to enter obstacle right state
// inputs:  none
// outputs: true - needs to enter obstacle right state, 
//          false - stay in same state
uint8_t Check_Obstacle_Right(void){
  if(IR2_Data < IR2_DIST_MIN){
		return true;
	}
	else{
		return false;
	}
}


//******** Check_Obstacle_Left **************
// Checks if robot needs to enter obstacle left state
// inputs:  none
// outputs: true - needs to enter obstacle left state, 
//          false - stay in same state
uint8_t Check_Obstacle_Left(void){
  if(IR1_Data < IR1_DIST_MIN){
		return true;
	}
	else{
		return false;
	}	
}


//******** Check_Reverse_Right **************
// Checks if robot needs to enter reverse right state
// inputs:  none
// outputs: true - needs to enter reverse right state, 
//          false - stay in same state
uint8_t Check_Reverse_Right(void){
  if(Bumper1_Data){
		return true;
	}
	else{
		return false;
	}
}


//******** Check_Reverse_Left **************
// Checks if robot needs to enter reverse left state
// inputs:  none
// outputs: true - needs to enter reverse left state, 
//          false - stay in same state
uint8_t Check_Reverse_Left(void){
  if(Bumper0_Data){
		return true;
	}
	else{
		return false;
	}
}

//******** Check_Reverse_Right_Clear **************
// Checks if robot needs to enter reverse right state
// inputs:  none
// outputs: true - needs to enter reverse right state, 
//          false - stay in same state
uint8_t Check_Reverse_Right_Clear(void){
  if(IR2_Data >= REVERSE_CLEAR_DIST){
		return true;
	}
	else{
		return false;
	}
}


//******** Check_Reverse_Left_Clear **************
// Checks if robot needs to enter reverse left state
// inputs:  none
// outputs: true - needs to enter reverse left state, 
//          false - stay in same state
uint8_t Check_Reverse_Left_Clear(void){
  if(IR1_Data >= REVERSE_CLEAR_DIST){
		return true;
	}
	else{
		return false;
	}
}

