// Interpreter.c
// Runs on TM4C123 
// Implements an interpreter using the UART serial port and interrupting I/O. 
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// March 20th, 2016

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "Interpreter.h"
#include "UART.h"
#include "Lab7_Sensor.h"
#include "Interrupts.h"
#include "OS.h"
#include "Ping.h"
#include "CAN_4C123\can0.h"
#include "IR.h"

cmdTable Commands[] = {        // CMD_LEN defines max command name length
	{"help",     cmdHelp,        "Displays all available commands."},
  {"ping",     cmdPing,        "Control ping sensors."},
	{"can",      cmdCAN,         "Control CAN"},
	{"ir",			 cmdIR,					 "Control IR sensors"}
};

char cmd[CMD_LEN+1];   // string to store command line inputs. +1 for null.
// *********************************************************************


void Interpreter_Init(void){
	printf("\n\n\r******************************************************\n\r");
	printf("                  Welcome to bOS.\n\r");
  printf("          Running software for sensor board\n\r");
	printf("         Type \"help\" for a list of commands.\n\r");
	printf("******************************************************\n\r");
}


//---------Interpreter_Parse-------
// Compare user's input with table of 
// available commands.
void Interpreter_Parse(void){
	printf("\n\r--Enter Command--> ");
	UART_InStringNL(cmd, CMD_LEN);  
  int i = NUM_OF(Commands);
  while(i--){
    if(!strcmp(cmd,Commands[i].name)){  // check for valid command
      Commands[i].func();
      return;
    }
  }
	printf("Command not recognized.\n\r");
}

	
//------------cmdHelp-----------------
// Display all the available commands
void cmdHelp(void){
	printf("Below is a list of availble commands:\n\r");
	// output formating and display command name
	for(int i = 0; i < NUM_OF(Commands); i++){
		if(i+1 <10) printf("    ");                   
		else if(i+1 < 100) printf("   ");     
		printf("%d",i+1);                             
		printf(") ");                         
		// display command name
		printf("%s",(char*)Commands[i].name);  
    // output formating		
		for(int j = strlen(Commands[i].name); j<CMD_LEN ; j++){
		  printf("-");                             
		}
		// display command tag
    printf("%s\n\r",(char*)Commands[i].tag);     
	}
}


//------------cmdPing-----------------
// Control the ping sensors.
void cmdPing(void){
	if(FirstRun){                                            // only allowed to add ping threads once
		FirstRun = 0;			                           
		OS_AddThread(&Ping1, 128, 1);                          // sends a pulse to the ping sensor #1, 10 times a second
		OS_AddThread(&PingDisplay, 128, 6);                    // display ping results
		printf("Ping sensors added. Displaying data to ST7735.\n\r");
	}
	else{
		printf("Error, Ping sensors already added.\n\r");
	}	
}


//------------cmdCAN-----------------
// Control CAN.
void cmdCAN(void){
	printf("Choose a command for CAN:\n\r");
	printf("    (1) Get Data - Get the data received in FIFO.\n\r");
	printf("    (2) Send Data - Send four bytes of data.\n\r");
	printf("    (3) Lost Packets - Display how many packets were lost.\n\r");

	printf("--Enter Command #--> ");
	uint32_t selection = UART_InUDec();
	printf("\n\r");
	
	switch(selection){
		// View data
		 case(1):{
			  uint8_t data[8];
			  unsigned long counter = 1;
			  printf("The data in the CAN0 fifo is as follows:\n\r");
			  if(CAN0PutPt!=CAN0GetPt){//if there is one
					do{//for all data in fifo
					  data[0] = CAN0_Fifo_Get();//Byte 0
			      data[1] = CAN0_Fifo_Get();//Byte 1
			      data[2] = CAN0_Fifo_Get();//Byte 2
			      data[3] = CAN0_Fifo_Get();//Byte 3
						data[4] = CAN0_Fifo_Get();//Byte 4
			      data[5] = CAN0_Fifo_Get();//Byte 5
			      data[6] = CAN0_Fifo_Get();//Byte 6
//			      data[7] = CAN0_Fifo_Get();//Byte 7
						//get data and print it
			      printf("Output %lu IR0 = %u\n\r", counter, data[0]);
						printf("Output %lu IR1 = %u\n\r", counter, data[1]);
						printf("Output %lu IR2 = %u\n\r", counter, data[2]);
						printf("Output %lu IR3 = %u\n\r", counter, data[3]);
						printf("Output %lu Ping1 = %u\n\r", counter, data[4]);
						printf("Output %lu Bumper0 = %u\n\r", counter, data[5]);
						printf("Output %lu Bumper1 = %u\n\r", counter, data[6]);
//						printf("Output %lu Start = %u\n\r\n\r", counter, data[7]);
						counter++;//increment counter
					}while(CAN0PutPt!=CAN0GetPt);
				}
				else{//if there is none
					printf("none");
					printf("\n\r");
				}
        break;
    }
	 // Send data
		case(2):{
			uint8_t data[8];
			uint32_t input;
			for(int i = 0; i < 8; i++){//for Bytes 0-7
				switch(i){//for every different sensor
					case 0:
			      printf("What is the input from IR0 (0-255): ");
					  break;
					case 1:
			      printf("\n\rWhat is the input from IR1 (0-255): ");
					  break;
					case 2:
			      printf("\n\rWhat is the input from IR2 (0-255): ");
					  break;
					case 3:
			      printf("\n\rWhat is the input from IR3 (0-255): ");
					  break;
					case 4:
			      printf("\n\rWhat is the input from Ping1 (0-255): ");
					  break;
					case 5:
			      printf("\n\rWhat is the input from Bumper0 (0-255): ");
					  break;
					case 6:
			      printf("\n\rWhat is the input from Bumper1 (0-255): ");
					  break;
//					case 7:
//			      printf("\n\rWhat is the input from Start (0-255): ");
//					  break;
				}
			  input=UART_InUDec();
			  if(input>255){
				  printf("\n\rImproper selection\n\r"); 
			    return;
			  }
			  else{
				  data[i] = input&0x000000FF;//store the byte
			  }
		  }
			CAN0_SendData(data);//send 8 bytes of data
			printf("\n\rYour inputs were sent.\n\r");
      break;
    }
		// Lost packets
    case(3):{
      printf("\n\r%lu packets have been lost.\n\r", PacketLost);
      break;
    }
    default: {
      printf("Improper selection\n\r"); 
      return;
    }
	}
}


//------------cmdIR-----------------
// Control the IR sensors.
void cmdIR(void) {
    #ifdef DEBUG
        OS_AddThread(&IRDisplay, 128, 6);										// display IR results
    #endif
    printf("\n\rDisplaying IR sensosr.\n\r");
}
