// Controller.h
// Runs on TM4C123
// Feedback controller for robot's motor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 26th, 2016

////////////////////////////////////////////////////////////////////////////////
// User Settings

#define SPEED_MIN 60
#define SPEED_MAX 100
// Gains here are scaled up x1000 for increased resolution
#define KP_E 1500   // default = 0
#define KI_E 0      // default = 0
#define KD_E 500    // default = 0
#define KP_F 1500   // default = 0
#define KI_F 0      // default = 0
#define KD_F 500    // default = 0
#define KP_O 1500   // default = 2000
#define KI_O 0      // default = 0
#define KD_O 500    // default = 500
#define KP_S 2000   // default = 2000
#define KI_S 0      // default = 0
#define KD_S 2000   // default = 2000

////////////////////////////////////////////////////////////////////////////////
// simpleController()
// Runs simple PID controllers to balance pairs of symmetrical IR sensors.
// Input: new sensor data
// Output: motor speed and servo position commands

void simpleController(void);
