// IR.h
// Runs on TM4C123
// Background thread using Timer1A to execute the function that getting the
// data from pre-initialized ADC, to implement a SW trigger ADC
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 8th, 2016


//********************Constants*********************************************
#define IR_64               1       //64 sampling average
#define IR_32               0       //32 sampling average
#define IR_4_INIT           0       //channel 0 PE3 
#define IR_5_INIT           1       //channel 1 PE2
#define IR_6_INIT           2       //channel 1 PE1
#define IR_7_INIT           3       //channel 1 PE0
#define FREQ                50      //Hz
#define IR_FREQ_64_10       64*FREQ //for FFT with 64 sampling average
#define IR_FREQ_32_10       32*FREQ //for FFT with 32 sampling average
//**************************************************************************


//********************Externs***********************************************
extern uint8_t IR_IRToDisplay;  // determines which ping sensor's data to display in PingDisplay()
extern long x0[64],y0[64];      // input and output arrays for FFT
extern long x1[64],y1[64];      // input and output arrays for FFT
extern long x2[64],y2[64];      // input and output arrays for FFT
extern long x3[64],y3[64];      // input and output arrays for FFT
extern unsigned long volatile IR4_Dist, IR5_Dist, IR6_Dist, IR7_Dist;
//**************************************************************************


//********************Prototypes********************************************
void Producer1(unsigned long data);
void Producer2(unsigned long data);
void Producer3(unsigned long data);
void Producer4(unsigned long data);
void Consumer1(void);
void Consumer2(void);
void Consumer3(void);
void Consumer4(void);
void IR_Compute_Period(void);
unsigned long IRCompute(unsigned long DCcomponent);
//**************************************************************************


//******** IR_Init_HW *************** 
void IR_Init_HW(void);


//******** IRDisplay *************** 
// Foreground thread.
// Display IR sensor readings 10 times a second
// on the ST7735 as distance vs. time.
// inputs:  none
// outputs: none
void IRDisplay(void);

//******** IR_Init_SW *************** 
void IR_Init_SW(void);


//******** IRAccuracy *************** 
// Foreground thread.
// Initalizes sensor, collects data, and displays results. 
// inputs:  none
// outputs: none
void IRAccuracy(void);
