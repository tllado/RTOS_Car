// Controller.h
// Runs on TM4C123
// Feedback controller for robot's motor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 26th, 2016

////////////////////////////////////////////////////////////////////////////////
// User Settings

#define SPEED_MIN 60
#define SPEED_MAX 100
// Gains here are scaled up x1000 for increased resolution
#define KP_SIDE 2000 // default = 2000
#define KI_SIDE 0    // default = 0
#define KD_SIDE 2000 // default = 2000
#define KP_FRNT 2000 // default = 2000
#define KI_FRNT 0    // default = 0
#define KD_FRNT 500  // default = 0
// #define damp   0	// damping factor, scaled up x1000

#define REACTION_DISTANCE   50      // cm
#define PING_INCREASE       3000    // max factor = (x/1000)^2

////////////////////////////////////////////////////////////////////////////////
// simpleController()
// Runs simple PID controllers to balance pairs of symmetrical IR sensors.
// Input: new sensor data
// Output: motor speed and servo position commands

void simpleController(void);
