// Motors.h
// Runs on TM4C123 
// Initiates and updates PWM signals on PB4, PB5, PB6, PB7, PD0, and PD1 pins
//  for motor and servo control.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// 2016.04.26

///////////////////////////////////////////////////////////////////////////////
// motorInit()
// Initializes PB4,PB5,PB6,PB7 at motFrq frequency and 0 duty cycle.
//  Initializes PD0,PD1 at srvFrq frequency and 1.5ms duty cycle.
// Input: none
// Output: none

void motorInit(void);

///////////////////////////////////////////////////////////////////////////////
// motorUpdate()
// Converts two values representing vehicle speed and turning rate into three
//  values representing two drive motor speeds and servo position.
// Input: two int values representing speed and turning rate
// Output: none

void motorUpdate(int speed, int turn);
