// Controller.c
// Runs on TM4C123
// Feedback controller for robot's motor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 26th, 2016

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "Controller.h"
#include "Lab7_Motor.h"
#include "Motors.h"
#include "Profiler.h"

////////////////////////////////////////////////////////////////////////////////
// Median()
// Very basic median filter.
// Input: three integers
// Output: Single integer, median of input values

int Median(int n1, int n2, int n3) {
    int n0 = 0;
    if(n1 > n2) {
        n0 = n1;
        n1 = n2;
        n2 = n0;
    }
    if(n2 > n3) {
        n0 = n2;
        n2 = n3;
        n3 = n0;
    }
    if(n1 > n2) {
        n0 = n1;
        n1 = n2;
        n2 = n0;
    }
    return n2;
}

////////////////////////////////////////////////////////////////////////////////
// calcError()
// Calculates P/I/D error values for symmetric pairs of IR sensors.
// Input: none, acts on global variables
// Output: none, acts on global variables

int IRS_EP, IRS_EI, IRS_ED, IRS_E1, IRS_E2, IRS_E3, IRS_Prev,
    IRF_EP, IRF_EI, IRF_ED, IRF_E1, IRF_E2, IRF_E3, IRF_Prev;

void calcError(void) {
    // Side IR Sensors
    IRS_EP = (int)IR0_Data - (int)IR3_Data;		// calc P Error
   
    IRS_EI += IRS_EP;                 				// calc I Error

    IRS_E3 = IRS_E2;                  				// calc D Error
    IRS_E2 = IRS_E1;
    IRS_E1 = IRS_EP - IRS_Prev;
    IRS_ED = Median(IRS_E1, IRS_E2, IRS_E3);
    IRS_Prev = IRS_EP;

    // Front IR Sensors
    IRF_EP = (int)IR1_Data - (int)IR2_Data;   // calc P Error
    
    IRF_EI += IRF_EP;                 				// calc I Error
    
    IRF_E3 = IRF_E2;                  				// calc D Error
    IRF_E2 = IRF_E1;
    IRF_E1 = IRF_EP - IRF_Prev;
    IRF_ED = Median(IRF_E1, IRF_E2, IRF_E3);
    IRF_Prev = IRF_EP;
}

////////////////////////////////////////////////////////////////////////////////
// pingMultiplier()
// Increases motor control gains as forward-facing ping sensor distance
//  decreases
// Input: none, acts on global variables
// Output: none, acts on global variables

uint32_t KpS, KiS, KdS, KpF, KiF, KdF, pingScale;

void pingMultiplier() {
    KpF = KP_FRNT;
    KiF = KI_FRNT;
    KdF = KD_FRNT;
    KpS = KP_SIDE;
    KiS = KI_SIDE;
    KdS = KD_SIDE;

    if(Ping1_Data < REACTION_DISTANCE) {
        pingScale = ((REACTION_DISTANCE - Ping1_Data)/REACTION_DISTANCE        \
                    *PING_INCREASE/1000)^2 + 1000;
        KdF = KD_FRNT*pingScale/1000;
    }
}

////////////////////////////////////////////////////////////////////////////////
// PID()
// Very basic PID controller. Uses global variables and combines the results of
//  two PID calculations.
// Input: K and EDor values for IRS and IRF
// Output: Combined control value for both IRS andIRF

int PID() {
    int ctrlSide = (KpS*IRS_EP + KiS*IRS_EI + KdS*IRS_ED)/1000;
    int ctrlFrnt = (KdF*IRF_EP + KiF*IRF_EI + KdF*IRF_ED)/1000;
    return ctrlFrnt + ctrlSide;
}

////////////////////////////////////////////////////////////////////////////////
// simpleController()
// Runs simple PID controllers to balance pairs of symmetrical IR sensors.
// Input: new sensor data
// Output: motor speed and servo position commands

void simpleController(void) {
    calcError();
    pingMultiplier();
    int direction = PID();
    int speed = SPEED_MAX - (SPEED_MAX - SPEED_MIN)*abs(direction)/127;
    motorUpdate(speed, direction);
}
