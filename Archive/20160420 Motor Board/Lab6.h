// Lab6.c
// Runs on TM4C123
// Real Time Operating System for Lab 6
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 3rd, 2016


//**********************Compilter Directives********************************
//Only one of these should be uncommented at any given time  
//#define MAIN          
#define DEBUG       
// *************************************************************************

//********************Constants*********************************************
#define ADC_SAMPLE_RATE 400          // producer/consumer sampling rate
#define PWM_FREQ        40000        // PWM frequency. 40000 = 500Hz
//**************************************************************************

//********************Externs***********************************************
extern uint16_t DutyPWM0;        // duty cycle for motor
//**************************************************************************


//********************Prototypes********************************************

//**************************************************************************


