// Ping.c
// Runs on TM4C123
// Use Timer3A in 24-bit edge time mode to request interrupts on the 
// rising and falling edge of PB2 (T3CCP0), to measure pulse width.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 4th, 2016


#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "PLL.h"
#include "Ping.h"
#include "ST7735.h"
#include "OS.h"
#include "Interrupts.h"
#include "Profiler.h"
#include "SysTick.h"
                           
//******************** Globals ***********************************************
volatile uint32_t StartTime1;          // Timer count on rising edge for Ping #1.
volatile uint32_t StartTime3;          // Timer count on rising edge for Ping #3.
volatile uint32_t PulseTime1;          // Time of Ping #1's pulse (.1ms)
volatile uint32_t PulseTime3;          // Time of Ping #3's pulse (.1ms)
uint8_t PingToDisplay;        // determines which ping sensor's data to display in PingDisplay()
//****************************************************************************


//******** Ping_Init *************** 
// Initializes ping timers for 24-bit edge time mode and request 
// interrupts on the rising and falling edge of specified CCPO pin.
// Max pulse measurement is (2^24-1)*12.5ns = 209.7151ms
// Min pulse measurement determined by time to run ISR, which is about 1us
// *Note - PF4 is shared with OS_AddButtonTask().  Comment out PF4 init if using SW1.
// inputs:  none
// outputs: none
void Ping_Init(void){
	// Initalize CCPO pins that cause interrupts
	// PB2 & PB4
  SYSCTL_RCGCGPIO_R |= 0x02;             // activate clock for port B      
	while((SYSCTL_PRGPIO_R&0x02) == 0){};  // allow time for clock to stabilize	
  GPIO_PORTB_DIR_R &= ~0x14;             // make PB2 & PB4 input
  GPIO_PORTB_AFSEL_R |= 0x14;            // enable alt funct on PB2 & PB4
  GPIO_PORTB_DEN_R |= 0x14;              // enable digital I/O on PB2 & PB4
  GPIO_PORTB_AMSEL_R &= ~0x14;           // disable analog functionality on PB2 & PB4
	GPIO_PORTB_PDR_R |= 0x14;              // enable weak pull-down on PB2 & PB4
  GPIO_PORTB_PCTL_R &= 0xFFF0F0FF;       // configure PB2 & PB4 as T0CCP0
  GPIO_PORTB_PCTL_R |= 0x00070700;       // configure PB2 & PB4 as T0CCP0
		
	// PF4
//  SYSCTL_RCGCGPIO_R |= 0x20;             // activate clock for port F 
//  while((SYSCTL_PRGPIO_R&0x20) == 0){};  // allow time for clock to stabilize	
//  GPIO_PORTF_DIR_R &= ~0x10;             // make PF4 input		
//	GPIO_PORTF_AFSEL_R |= 0x10;            // enable alt funct on PF4
//	GPIO_PORTF_DEN_R |= 0x10;              // enable digital I/O on PF4
		
		
		
  // Initalize timers used for input capture
	// Timer1
	SYSCTL_RCGCTIMER_R |= 0x02;            // activate TIMER1  
	while((SYSCTL_PRTIMER_R&0x02) == 0){}; // allow time for clock to stabilize	
  TIMER1_CTL_R &= ~TIMER_CTL_TAEN;       // disable timer1A during setup
  TIMER1_CFG_R = TIMER_CFG_16_BIT;       // configure for 16-bit timer mode                                   
  TIMER1_TAMR_R = (TIMER_TAMR_TACMR|TIMER_TAMR_TAMR_CAP); // configure for 24-bit capture mode                                 
	TIMER1_CTL_R |= TIMER_CTL_TAEVENT_NEG; // configure as neg edge event
  TIMER1_TAILR_R = TIMER_TAILR_TAILRL_M; // start value
  TIMER1_TAPR_R = 0xFF;                  // activate prescale, creating 24-bit
  TIMER1_IMR_R |= TIMER_IMR_CAEIM;       // enable capture match interrupt
  TIMER1_ICR_R = TIMER_ICR_CAECINT;      // clear timer1A capture match flag
  TIMER1_CTL_R |= TIMER_CTL_TAEN;        // enable timer1A 16-b, +edge timing, interrupts
                                   
  NVIC_PRI5_R = (NVIC_PRI5_R&0xFFFF00FF)|0x00002000; // Timer1A priority = 1, top 3 bits
  NVIC_EN0_R = NVIC_EN0_INT21;           // enable interrupt 21 in NVIC. 
		
	// Timer3
	SYSCTL_RCGCTIMER_R |= 0x08;            // activate TIMER3  
	while((SYSCTL_PRTIMER_R&0x08) == 0){}; // allow time for clock to stabilize	
  TIMER3_CTL_R &= ~TIMER_CTL_TAEN;       // disable timer3A during setup
  TIMER3_CFG_R = TIMER_CFG_16_BIT;       // configure for 16-bit timer mode                                   
  TIMER3_TAMR_R = (TIMER_TAMR_TACMR|TIMER_TAMR_TAMR_CAP); // configure for 24-bit capture mode                                 
	TIMER3_CTL_R |= TIMER_CTL_TAEVENT_NEG; // configure as neg edge event
  TIMER3_TAILR_R = TIMER_TAILR_TAILRL_M; // start value
  TIMER3_TAPR_R = 0xFF;                  // activate prescale, creating 24-bit
  TIMER3_IMR_R |= TIMER_IMR_CAEIM;       // enable capture match interrupt
  TIMER3_ICR_R = TIMER_ICR_CAECINT;      // clear timer3A capture match flag
  TIMER3_CTL_R |= TIMER_CTL_TAEN;        // enable timer3A 16-b, +edge timing, interrupts
                                   
  NVIC_PRI8_R = (NVIC_PRI8_R&0x00FFFFFF)|0x20000000; // Timer3A priority = 1, top 3 bits
  NVIC_EN1_R = NVIC_EN1_INT35;           // enable interrupt 35 in NVIC.  Uses EN1 since IRQ > 31	
}


// ******** Timer1A_Handler **************
// Captures pulse width.
void Timer1A_Handler(void){
	TIMER1_ICR_R = TIMER_ICR_CAECINT;                                   // acknowledge timer1A capture match
	int32_t status = StartCritical();

	// capture time on falling edge
	uint32_t endTime = TIMER1_TAR_R;
	PulseTime1 = OS_TimeDifference(StartTime1,endTime)/8000;            // how long the pulse was high (.1ms units)
	//PulseTime1 = OS_TimeDifference(StartTime,endTime)/80000;          // how long the pulse was high (ms units)	
	
	// set pin back in default state for next signal pulse
	GPIO_PORTB_PDR_R |= 0x10;                                           // enable weak pull-down on PB4
	EndCritical(status);
}


// ******** Timer3A_Handler **************
// Captures pulse width.
void Timer3A_Handler(void){
	TIMER3_ICR_R = TIMER_ICR_CAECINT;                                   // acknowledge timer3A capture match
	int32_t status = StartCritical();

	// capture time on falling edge
	uint32_t endTime = TIMER3_TAR_R;
	PulseTime3 = OS_TimeDifference(StartTime3,endTime)/8000;            // how long the pulse was high (.1ms units)
	//PulseTime3 = OS_TimeDifference(StartTime,endTime)/80000;          // how long the pulse was high (ms units)	
	
	// set pin back in default state for next signal pulse
	GPIO_PORTB_PDR_R |= 0x04;                                           // enable weak pull-down on PB2
	EndCritical(status);
}


//******** SendPulse *************** 
// Issue a 40kHz sound pulse by sending a short 
// logical high burst ~(5us) to the ping sensor.
// Called from PulsePing().
// inputs:  pingID - the ID of the ping sensor you wish to send a pulse to
// outputs: none
void SendPulse(uint8_t pingID){
	int32_t status = StartCritical();
	switch (pingID){
    // ping sensor 1
	  case(1):{
			TIMER1_CTL_R &= ~TIMER_CTL_TAEN;    // disable timer1A during pulse
			
			// set PB4 as a GPIO output
			GPIO_PORTB_DIR_R |= 0x10;           // make PB4 output
			GPIO_PORTB_AFSEL_R &= ~0x10;        // disable alt funct on PB4
			GPIO_PORTB_PCTL_R &= 0xFFF0FFFF;    // configure PB4 as GPIO
			GPIO_PORTB_PCTL_R |= 0x00000000;    // configure PB4 as GPIO
		
			// create 5us pulse
			PB4 = 0x10;
			SysTick_Wait(400);                  // 5us / 12.5ns = 400
			PB4 = 0x00;	
			break;
		}
		
		// ping sensor 2
		case(2):{
			
			
			break;
		}
		
		// ping sensor 3
		case(3):{
			TIMER3_CTL_R &= ~TIMER_CTL_TAEN;    // disable timer3A during pulse
			
			// set PB2 as a GPIO output
			GPIO_PORTB_DIR_R |= 0x04;           // make PB2 output
			GPIO_PORTB_AFSEL_R &= ~0x04;        // disable alt funct on PB2
			GPIO_PORTB_PCTL_R &= 0xFFFFF0FF;    // configure PB2 as GPIO
			GPIO_PORTB_PCTL_R |= 0x00000000;    // configure PB2 as GPIO
			// create 5us pulse
			PB2 = 0x04;
			SysTick_Wait(400);                  // 5us / 12.5ns = 400
			PB2 = 0x00;	
			break;
		}
	}
	EndCritical(status);
}


//******** ConfigForCapture *************** 
// Configure PB2 as an input capture pin with a pull up resistor
// Called from PulsePing().
// inputs:  pingID - the ID of the ping sensor you wish configure for input capture
// outputs: none
void ConfigForCapture(uint8_t pingID){
	int32_t status = StartCritical();
	switch (pingID){
    // ping sensor 1
	  case(1):{
			GPIO_PORTB_DIR_R &= ~0x10;             // make PB4 input
			GPIO_PORTB_AFSEL_R |= 0x10;            // enable alt funct on PB4
			GPIO_PORTB_PUR_R |= 0x10;              // enable weak pull-up on PB4
			GPIO_PORTB_PCTL_R &= 0xFFF0FFFF;       // configure PB4 as T0CCP0
			GPIO_PORTB_PCTL_R |= 0x00070000;       // configure PB4 as T0CCP0
			TIMER1_ICR_R = TIMER_ICR_CAECINT;      // clear timer1A capture match flag
			TIMER1_CTL_R |= TIMER_CTL_TAEN;        // re-enable timer1A since SendPulse disabled it
			StartTime1 = TIMER1_TAR_R;             // set startTime for measuring Ping pulse
			break;
		}
		
		// ping sensor 2
		case(2):{
			
			
			break;
		}
		
		// ping sensor 3
		case(3):{
			GPIO_PORTB_DIR_R &= ~0x04;             // make PB2 input
			GPIO_PORTB_AFSEL_R |= 0x04;            // enable alt funct on PB2
			GPIO_PORTB_PUR_R |= 0x04;              // enable weak pull-up on PB2
			GPIO_PORTB_PCTL_R &= 0xFFFFF0FF;       // configure PB2 as T0CCP0
			GPIO_PORTB_PCTL_R |= 0x00000700;       // configure PB2 as T0CCP0
			TIMER3_ICR_R = TIMER_ICR_CAECINT;      // clear timer3A capture match flag
			TIMER3_CTL_R |= TIMER_CTL_TAEN;        // re-enable timer3A since SendPulse disabled it
			StartTime3 = TIMER3_TAR_R;             // set startTime for measuring Ping pulse
			break;
		}
	}
	EndCritical(status);
}


//******** Ping1 *************** 
// Foreground thread.
// Send a pulse to the designated Ping sensor 10 times a second.
// Capture the sonar response time and calculate distance.
// inputs:  none
// outputs: none
void Ping1(void){
	uint8_t pingID = 1;
	// send pulses 10 times a second
	while(1){
		SendPulse(pingID);         // send a single pulse to the Ping sensor
		SysTick_Wait(60000);       // wait for holdoff time = 750 us.  
		ConfigForCapture(pingID);  // configure pin to capture pulse width
		
		// sleep 100ms to accomplish 10Hz pinging
		OS_Sleep(100);   
	}
}


//******** Ping3 *************** 
// Foreground thread.
// Send a pulse to the designated Ping sensor 10 times a second.
// Capture the sonar response time and calculate distance.
// inputs:  none
// outputs: none
void Ping3(void){
	uint8_t pingID = 3;
	// send pulses 10 times a second
	while(1){
		SendPulse(pingID);         // send a single pulse to the Ping sensor
		SysTick_Wait(60000);       // wait for holdoff time = 750 us.  
		ConfigForCapture(pingID);  // configure pin to capture pulse width
		
		// sleep 100ms to accomplish 10Hz pinging
		OS_Sleep(100);   
	}
}


//******** PingDisplay *************** 
// Foreground thread.
// Display ping sensor readings 10 times a second
// on the ST7735 as distance vs. time.
// inputs:  none
// outputs: none
void PingDisplay(void){
	// setup plot screen
	ST7735_SetCursor(0,0);
	ST7735_OutString("Distance Vs. Time");
	ST7735_PlotClear(0,63);     // graph range from 0 to 63
	// plot results
	while(1){                   // never dies
    uint32_t chosenPulseTime;
		switch (PingToDisplay){
			case(1):{
        chosenPulseTime = PulseTime1;
				break;
			}

			case(2):{

				break;
			}

			case(3):{
        chosenPulseTime = PulseTime3;
				break;
			}
		}
		
		uint32_t dist = C_AIR * chosenPulseTime/2;        // distance in cm
		if(dist<300){                                     // max sensor reading is 3 meters
			dist+=2;                                        // calibration
			OS_bWait(&LCDFree);                             // capture resource
			ST7735_FillRect(66, 10, 60, 10, ST7735_BLACK);  // original
			OS_bSignal(&LCDFree);                           // release resource
			ST7735_Message(0,1,"dist(cm) = ",dist);         // display distance to screen
			ST7735_PlotLine(dist/5);                        // divide to fit to screen
			ST7735_PlotNextErase();
		}
		// sleep 100ms to accomplish 10Hz pinging
		OS_Sleep(100);  
	}
}


//******** PingAccuracy *************** 
// Foreground thread.
// Initalizes sensor, collects data, and displays results. 
// Used to determine accuracy of sensors
// inputs:  none
// outputs: none
uint8_t debounce = 0;               // only add one thread when button is pressed
uint8_t testNum = 0;                // current iteration of test
uint32_t PingBuff[5];               // buffer to store data points
uint8_t PingID = 3;                 // ID of ping sensor you are testing. Ping 1 thru 3.
void PingAccuracy(void){
	int32_t status = StartCritical();
	if(!debounce){
		debounce = 1;
		testNum++;
		EndCritical(status);
	  // send pulses 10 times a second
	  for(int i = 0; i<5 ;i++){
		  SendPulse(PingID);                          // send a single pulse to the Ping sensor
		  SysTick_Wait(60000);                        // wait for holdoff time = 750 us.  
		  ConfigForCapture(PingID);                   // configure pin to capture pulse width
		
		  // sleep 100ms to accomplish 10Hz pinging
		  OS_Sleep(100); 
			uint8_t pulseTime;
			switch (PingID){
				// ping sensor 1 (J10Y header)
				case(1):{
					pulseTime = PulseTime1;
					break;
	    	}
				// ping sensor 2 (J12Y header)
			  case(2):{
					//pulseTime = PulseTime2;
					break;
	    	}
				// ping sensor 3 (J11Y header)
				case(3):{
          pulseTime = PulseTime3;
					break;
	    	}
			}
			int32_t status = StartCritical();
			if((C_AIR * (pulseTime>>1)) < 300){          // distance in cm
			  PingBuff[i] = pulseTime;                   // store to buffer for display later
				EndCritical(status);
			}
			else{
				i--;                                       // erroneous data. retry data point
				EndCritical(status);
			}
		}			
		ST7735_FillScreen(0);            // set screen to black
		ST7735_Message(0,0,"Test # ", testNum);
		for(int i = 0; i<5; i++){
			uint32_t dist = C_AIR * (PingBuff[i]>>1);    // distance in cm
		  ST7735_Message(0,i+1,"Dist(cm) = ", dist);   // display results
		}
		debounce = 0;
	}
	EndCritical(status);
	OS_Kill();
}
