// Timer5.h
// Runs on TM4C123 
// Use TIMER5 in 32-bit periodic mode.  Used for calculating time.  
// Does not trigger interrupts.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// February 21, 2016


// ***************** Timer5_Init ****************
// Activate Timer5A
// Inputs:  period in units (1/clockfreq)
// Outputs: none
void Timer5_Init(unsigned long period);

