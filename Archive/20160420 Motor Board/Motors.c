// Motors.c
// Runs on TM4C123 
// Initiates and updates PWM signals on PB4, PB5, PB6, PB7, PD0, and PD1 pins
//  for motor and servo control
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// 2016.04.20

///////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include "tm4c123gh6pm.h"

///////////////////////////////////////////////////////////////////////////////
// User Settings

const uint16_t motFrq = 800;        // Hz
const uint16_t srvFrq = 50;         // Hz
const uint32_t sysFrq = 80000000;   // Hz
const uint16_t PWMDiv = 32;         // System Clock ticks per PWM Clock tick

///////////////////////////////////////////////////////////////////////////////
// Global Variables

const uint16_t motPrdDef = sysFrq/PWMDiv/motFrq;
const uint16_t motDtyDef = 0;                   // idle
const uint16_t srvPrdDef = sysFrq/PWMDiv/srvFrq;
const uint16_t srvDtyDef = srvPrdDef * 3/2/20;  // 1.5ms, center

////////////////////////////////////////////////////////////////////////////////
// Motor Speed Lookup Tables
// These empirically derived lookup tables convert two integer values
//  representing speed and turning rate into three PWM values for the car's two
//  drive motors and one steering servo.

uint16_t Duty00A[256] = {
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,   53,   97,  134, 
     169,  200,  228,  259,  286,  313,  338,  366,  391,  414,  439,  463, 
     488,  509,  534,  558,  581,  603,  628,  650,  673,  697,  719,  744, 
     766,  789,  813,  836,  859,  881,  906,  928,  953,  975, 1000, 1023, 
    1047, 1070, 1094, 1119, 1142, 1166, 1191, 1213, 1238, 1263, 1284, 1309, 
    1334, 1358, 1381, 1406, 1428, 1453, 1478, 1500, 1525, 1548, 1572, 1597, 
    1619, 1644, 1666, 1691, 1713, 1738, 1759, 1784, 1806, 1830, 1853, 1875, 
    1900, 1922, 1944, 1969, 1991, 2013, 2036, 2059, 2081, 2105, 2128, 2150, 
    2173, 2197, 2219, 2241, 2264, 2288, 2309, 2334, 2356, 2380, 2403, 2425, 
    2450, 2472, 2497, 2520, 2544, 2569, 2592, 2616, 2641, 2666, 2689, 2714, 
    2739, 2764, 2789, 2816, 2841, 2866, 2891, 2917, 2944, 2969, 2995, 3022, 
    3047, 3073, 3100, 3125
};

uint16_t Duty00B[256] = {
       0, 2925, 2900, 2877, 2853, 2828, 2805, 2781, 2758, 2734, 2713, 2689,
    2666, 2644, 2622, 2600, 2578, 2556, 2534, 2513, 2491, 2469, 2447, 2427,
    2405, 2384, 2363, 2341, 2320, 2298, 2278, 2256, 2234, 2214, 2192, 2172,
    2150, 2128, 2108, 2086, 2066, 2044, 2022, 2000, 1978, 1956, 1934, 1914,
    1892, 1870, 1848, 1827, 1805, 1783, 1761, 1738, 1716, 1694, 1672, 1650,
    1628, 1606, 1583, 1561, 1538, 1516, 1494, 1472, 1450, 1427, 1405, 1381,
    1359, 1338, 1316, 1294, 1272, 1248, 1227, 1205, 1183, 1161, 1139, 1117,
    1095, 1073, 1052, 1031, 1009,  988,  966,  944,  923,  902,  881,  859,
     838,  817,  797,  775,  755,  733,  713,  691,  669,  648,  628,  606,
     584,  563,  541,  519,  497,  475,  453,  430,  406,  381,  358,  333,
     306,  281,  253,  222,  191,  156,  119,   70,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0
};

uint16_t Duty01A[256] = {
       0, 3125, 3094, 3063, 3031, 3003, 2972, 2944, 2916, 2889, 2863, 2834,
    2809, 2784, 2758, 2733, 2708, 2684, 2659, 2634, 2611, 2588, 2563, 2541,
    2517, 2494, 2472, 2448, 2425, 2403, 2380, 2358, 2334, 2313, 2291, 2269,
    2245, 2223, 2200, 2178, 2156, 2134, 2113, 2091, 2067, 2045, 2022, 2000,
    1978, 1956, 1933, 1909, 1888, 1866, 1842, 1819, 1797, 1773, 1750, 1728,
    1705, 1681, 1659, 1634, 1613, 1589, 1566, 1542, 1519, 1495, 1472, 1448,
    1425, 1402, 1378, 1356, 1331, 1309, 1284, 1263, 1238, 1216, 1192, 1169,
    1145, 1122, 1100, 1077, 1053, 1031, 1008,  984,  963,  941,  917,  894,
     872,  850,  828,  805,  783,  759,  738,  716,  694,  672,  648,  625,
     603,  581,  558,  534,  513,  488,  466,  441,  416,  391,  366,  341,
     313,  286,  256,  228,  194,  159,  119,   70,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0
};

uint16_t Duty01B[256] = {
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
       0,    0,    0,    0,    0,    0,    0,    0,    0,   69,  116,  156,
     194,  225,  256,  288,  316,  344,  369,  397,  422,  447,  472,  497,
     519,  544,  567,  591,  614,  638,  661,  684,  706,  730,  753,  775,
     798,  822,  844,  867,  891,  913,  936,  959,  981, 1006, 1028, 1052,
    1075, 1098, 1122, 1144, 1169, 1191, 1216, 1238, 1263, 1284, 1309, 1333,
    1356, 1380, 1403, 1427, 1450, 1473, 1497, 1520, 1544, 1567, 1591, 1614,
    1638, 1661, 1684, 1708, 1731, 1755, 1778, 1800, 1825, 1847, 1870, 1894,
    1916, 1939, 1963, 1984, 2008, 2031, 2053, 2078, 2100, 2122, 2147, 2169,
    2192, 2216, 2238, 2261, 2284, 2306, 2331, 2353, 2377, 2400, 2423, 2447,
    2470, 2494, 2517, 2541, 2566, 2589, 2613, 2638, 2661, 2684, 2709, 2734,
    2758, 2783, 2806, 2831, 2856, 2881, 2906, 2931, 2955, 2980, 3003, 3028,
    3053, 3077, 3100, 3125
};

uint16_t Duty03A[256] = {
    3850, 2700, 2709, 2718, 2727, 2736, 2745, 2754, 2763, 2772, 2781, 2791,
    2800, 2809, 2818, 2827, 2836, 2845, 2854, 2863, 2872, 2881, 2890, 2899,
    2908, 2917, 2926, 2935, 2944, 2954, 2963, 2972, 2981, 2990, 2999, 3008,
    3017, 3026, 3035, 3044, 3053, 3062, 3071, 3080, 3089, 3098, 3107, 3117,
    3126, 3135, 3144, 3153, 3162, 3171, 3180, 3189, 3198, 3207, 3216, 3225,
    3234, 3243, 3252, 3261, 3270, 3280, 3289, 3298, 3307, 3316, 3325, 3334,
    3343, 3352, 3361, 3370, 3379, 3388, 3397, 3406, 3415, 3424, 3433, 3443,
    3452, 3461, 3470, 3479, 3488, 3497, 3506, 3515, 3524, 3533, 3542, 3551,
    3560, 3569, 3578, 3587, 3596, 3606, 3615, 3624, 3633, 3642, 3651, 3660,
    3669, 3678, 3687, 3696, 3705, 3714, 3723, 3732, 3741, 3750, 3759, 3769,
    3778, 3787, 3796, 3805, 3814, 3823, 3832, 3841, 3850, 3859, 3868, 3877,
    3886, 3895, 3904, 3913, 3922, 3931, 3941, 3950, 3959, 3968, 3977, 3986,
    3995, 4004, 4013, 4022, 4031, 4040, 4049, 4058, 4067, 4076, 4085, 4094,
    4104, 4113, 4122, 4131, 4140, 4149, 4158, 4167, 4176, 4185, 4194, 4203,
    4212, 4221, 4230, 4239, 4248, 4257, 4267, 4276, 4285, 4294, 4303, 4312,
    4321, 4330, 4339, 4348, 4357, 4366, 4375, 4384, 4393, 4402, 4411, 4420,
    4430, 4439, 4448, 4457, 4466, 4475, 4484, 4493, 4502, 4511, 4520, 4529,
    4538, 4547, 4556, 4565, 4574, 4583, 4593, 4602, 4611, 4620, 4629, 4638,
    4647, 4656, 4665, 4674, 4683, 4692, 4701, 4710, 4719, 4728, 4737, 4746,
    4756, 4765, 4774, 4783, 4792, 4801, 4810, 4819, 4828, 4837, 4846, 4855,
    4864, 4873, 4882, 4891, 4900, 4909, 4919, 4928, 4937, 4946, 4955, 4964,
    4973, 4982, 4991, 5000
};

uint16_t RF00[256] = {
    1000, 1265, 1263, 1262, 1260, 1258, 1256, 1255, 1253, 1251, 1249, 1247,
    1246, 1244, 1242, 1240, 1238, 1236, 1235, 1233, 1231, 1229, 1227, 1225,
    1223, 1221, 1219, 1218, 1216, 1213, 1211, 1210, 1208, 1206, 1204, 1202,
    1200, 1198, 1196, 1194, 1192, 1190, 1187, 1185, 1183, 1181, 1179, 1177,
    1175, 1173, 1171, 1169, 1167, 1165, 1162, 1160, 1158, 1156, 1154, 1152,
    1150, 1148, 1146, 1143, 1141, 1139, 1137, 1135, 1132, 1130, 1128, 1126,
    1124, 1122, 1120, 1117, 1115, 1113, 1111, 1109, 1107, 1104, 1102, 1100,
    1098, 1095, 1093, 1091, 1089, 1087, 1084, 1082, 1080, 1078, 1076, 1073,
    1071, 1069, 1067, 1065, 1062, 1060, 1058, 1056, 1053, 1051, 1049, 1047,
    1045, 1042, 1040, 1038, 1036, 1033, 1031, 1029, 1027, 1025, 1022, 1020,
    1018, 1016, 1013, 1011, 1009, 1007, 1004, 1002, 1000,  998,  996,  993,
     991,  989,  987,  984,  982,  980,  978,  975,  973,  971,  969,  967,
     964,  962,  960,  958,  955,  953,  951,  949,  947,  944,  942,  940,
     938,  935,  933,  931,  929,  927,  924,  922,  920,  918,  916,  913,
     911,  909,  907,  905,  902,  900,  898,  896,  893,  891,  889,  887,
     885,  883,  880,  878,  876,  874,  872,  870,  868,  865,  863,  861,
     859,  857,  854,  852,  850,  848,  846,  844,  842,  840,  838,  835,
     833,  831,  829,  827,  825,  823,  821,  819,  817,  815,  813,  810,
     808,  806,  804,  802,  800,  798,  796,  794,  792,  790,  789,  787,
     784,  782,  781,  779,  777,  775,  773,  771,  769,  767,  765,  764,
     762,  760,  758,  756,  754,  753,  751,  749,  747,  745,  744,  742,
     740,  738,  737,  735
 };

uint16_t RF01[256] = {
    1000,  735,  737,  738,  740,  742,  744,  745,  747,  749,  751,  753,
     754,  756,  758,  760,  762,  764,  765,  767,  769,  771,  773,  775,
     777,  779,  781,  782,  784,  787,  789,  790,  792,  794,  796,  798,
     800,  802,  804,  806,  808,  810,  813,  815,  817,  819,  821,  823,
     825,  827,  829,  831,  833,  835,  838,  840,  842,  844,  846,  848,
     850,  852,  854,  857,  859,  861,  863,  865,  868,  870,  872,  874,
     876,  878,  880,  883,  885,  887,  889,  891,  893,  896,  898,  900,
     902,  905,  907,  909,  911,  913,  916,  918,  920,  922,  924,  927,
     929,  931,  933,  935,  938,  940,  942,  944,  947,  949,  951,  953,
     955,  958,  960,  962,  964,  967,  969,  971,  973,  975,  978,  980,
     982,  984,  987,  989,  991,  993,  996,  998, 1000, 1002, 1004, 1007,
    1009, 1011, 1013, 1016, 1018, 1020, 1022, 1025, 1027, 1029, 1031, 1033,
    1036, 1038, 1040, 1042, 1045, 1047, 1049, 1051, 1053, 1056, 1058, 1060,
    1062, 1065, 1067, 1069, 1071, 1073, 1076, 1078, 1080, 1082, 1084, 1087,
    1089, 1091, 1093, 1095, 1098, 1100, 1102, 1104, 1107, 1109, 1111, 1113,
    1115, 1117, 1120, 1122, 1124, 1126, 1128, 1130, 1132, 1135, 1137, 1139,
    1141, 1143, 1146, 1148, 1150, 1152, 1154, 1156, 1158, 1160, 1162, 1165,
    1167, 1169, 1171, 1173, 1175, 1177, 1179, 1181, 1183, 1185, 1187, 1190,
    1192, 1194, 1196, 1198, 1200, 1202, 1204, 1206, 1208, 1210, 1211, 1213,
    1216, 1218, 1219, 1221, 1223, 1225, 1227, 1229, 1231, 1233, 1235, 1236,
    1238, 1240, 1242, 1244, 1246, 1247, 1249, 1251, 1253, 1255, 1256, 1258,
    1260, 1262, 1263, 1265
};

///////////////////////////////////////////////////////////////////////////////
// motorInit()
// Initializes PB4,PB5,PB6,PB7 at motFrq frequency and 0 duty cycle.
//  Initializes PD0,PD1 at srvFrq frequency and 1.5ms duty cycle.
// Takes no input.
// Gives no output.

void motorInit(void) {
    // Initialize PB4, PB5, PB6, PB7, PD0, and PD1 for PWM
    SYSCTL_RCGCPWM_R |= SYSCTL_RCGCPWM_R0;  // activate clock for PWM Mod 0
    SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R1|SYSCTL_RCGCGPIO_R3;
                                            // activate clock for Port B
    while((SYSCTL_PRGPIO_R&SYSCTL_PRGPIO_R1) == 0){};
                                            // allow time to finish activating
    while((SYSCTL_PRGPIO_R&SYSCTL_PRGPIO_R3) == 0){};
                                            // allow time to finish activating
    GPIO_PORTB_AFSEL_R |= 0xF0;             // enable alt funct on PB4-7
    GPIO_PORTB_PCTL_R &= ~0xFFFF0000;       // configure PB4-7 as PWM
    GPIO_PORTB_PCTL_R |= 0x44440000;
    GPIO_PORTB_AMSEL_R &= ~0xF0;            // disable analog function on PB4-7
    GPIO_PORTB_DEN_R |= 0xF0;               // enable digital I/O on PB4-7
    GPIO_PORTD_AFSEL_R |= 0x03;             // enable alt funct on PD0-1
    GPIO_PORTD_PCTL_R &= ~0x000000FF;       // configure PD0-1 as PWM
    GPIO_PORTD_PCTL_R |= 0x00000044;
    GPIO_PORTD_AMSEL_R &= ~0x03;            // disable analog function on PD0-1
    GPIO_PORTD_DEN_R |= 0x03;               // enable digital I/O on PD0-1
        
    // Set PWM clock to fractin of system clock
    SYSCTL_RCC_R |= SYSCTL_RCC_USEPWMDIV;
    SYSCTL_RCC_R &= ~SYSCTL_RCC_PWMDIV_M;
    SYSCTL_RCC_R += SYSCTL_RCC_PWMDIV_32;
        
    // Setup PB4-7, PD0-1 for motor control
    PWM0_0_CTL_R = 0;                       // set countdown mode for Mod0 Blk0
    PWM0_1_CTL_R = 0;                       // set countdown mode for Mod0 Blk1
    PWM0_3_CTL_R = 0;                       // set countdown mode for Mod0 Blk3
    PWM0_0_GENA_R = (PWM_0_GENA_ACTCMPAD_ONE|PWM_0_GENA_ACTLOAD_ZERO);
    PWM0_0_GENB_R = (PWM_0_GENB_ACTCMPBD_ONE|PWM_0_GENB_ACTLOAD_ZERO);
    PWM0_1_GENA_R = (PWM_1_GENA_ACTCMPAD_ONE|PWM_1_GENA_ACTLOAD_ZERO);
    PWM0_1_GENB_R = (PWM_1_GENB_ACTCMPBD_ONE|PWM_1_GENB_ACTLOAD_ZERO);
    PWM0_3_GENA_R = (PWM_3_GENA_ACTCMPAD_ONE|PWM_3_GENA_ACTLOAD_ZERO);
    PWM0_3_GENB_R = (PWM_3_GENB_ACTCMPBD_ONE|PWM_3_GENB_ACTLOAD_ZERO);
                                            // define signal triggers
    PWM0_0_LOAD_R = motPrdDef;              // set counter reset value (period)
    PWM0_1_LOAD_R = motPrdDef;
    PWM0_3_LOAD_R = srvPrdDef;
    PWM0_0_CMPA_R = motDtyDef;              // set comparator value (duty)
    PWM0_0_CMPB_R = motDtyDef;
    PWM0_1_CMPA_R = motDtyDef;
    PWM0_1_CMPB_R = motDtyDef;
    PWM0_3_CMPA_R = srvDtyDef;
    PWM0_3_CMPB_R = srvDtyDef;
    PWM0_0_CTL_R |= PWM_0_CTL_ENABLE;       // start Mod 0 Blk 0
    PWM0_1_CTL_R |= PWM_1_CTL_ENABLE;       // start Mod 0 Blk 1
    PWM0_3_CTL_R |= PWM_3_CTL_ENABLE;       // start Mod 0 Blk 3
    PWM0_ENABLE_R |= (PWM_ENABLE_PWM0EN|PWM_ENABLE_PWM1EN|PWM_ENABLE_PWM2EN|\
    PWM_ENABLE_PWM3EN|PWM_ENABLE_PWM6EN|PWM_ENABLE_PWM7EN);
                                            // enable PWM Module 0
}

///////////////////////////////////////////////////////////////////////////////
// motorUpdate()
// Converts two motor speeds to four PWM duty values, converts one servo
//  position into one PWM duty cycle, and updates six corresponding registries.
// Takes input of three uint8_t values representing two motor speeds and one
//  servo position.
// Gives no output.

void motorUpdate(int speed, int turn){
    PWM0_0_CMPA_R = Duty00A[(speed - 128)*RF00[turn]/1000 + 128];
    PWM0_0_CMPB_R = Duty00B[(speed - 128)*RF00[turn]/1000 + 128];
                                    // Set left motor speed
    PWM0_1_CMPA_R = Duty01A[(speed - 128)*RF01[turn]/1000 + 128];
    PWM0_1_CMPB_R = Duty01B[(speed - 128)*RF01[turn]/1000 + 128];
                                    // Set right motor speed
    PWM0_3_CMPA_R = Duty03A[turn];  // Set servo positn
    PWM0_3_CMPB_R = 0;              // Not in use
}
