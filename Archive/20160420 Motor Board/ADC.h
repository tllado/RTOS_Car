// ADC.h
// Runs on TM4C123 
// Provides analog-to-digital functionality. 
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// January 23, 2016

// Analog channel inputs[0:11] on TM4C123:
// AIN0  - PE3 
// AIN1  - PE2 
// AIN2  - PE1 
// AIN3  - PE0 
// AIN4  - PD3 
// AIN5  - PD2 
// AIN6  - PD1 
// AIN7  - PD0 
// AIN8  - PE5
// AIN9  - PE4 
// AIN10 - PB4 
// AIN11 - PB5 

#define EIGHTY_MHZ 80000000


// *************** ADC_HWTrigger ********************
// Initializes a channel on ADC0 for timer0 sampling. 
// Samples are passed onto a user defined function
// The parameters below are modified:
// Timer0A: initialized and enabled
// Mode: 16-bit, down counting
// One-shot or periodic: periodic
// Interval value: programmable using 16-bit period
// Sample time is busPeriod*period
// Max sample rate: <=125,000 samples/second
// Sequencer 0 priority: 1st (lowest)
// Sequencer 1 priority: 2nd
// Sequencer 2 priority: 3rd
// Sequencer 3 priority: 4th (highest)
// SS3 triggering event: Timer0A
// SS3 1st sample source: programmable using variable 'channelNum' [0:11]
// SS3 interrupts: initialized and enabled.

// Inputs: channelNum - ADC channel input [0:11].
//         freq - sample frequency.
//         task - function pointer that accepts the collected ADC values.
// Output: none
void ADC_InitHWTrigger(uint8_t channelNum, uint32_t freq, void(*task)(unsigned long data));

// enable ADC seq3 interrupts
void ADC_Seq3Enable(void);

// disable ADC seq3 interrupts
void ADC_Seq3Disable(void);


//------------ADC_In------------
// Busy-wait Analog to digital conversion
// Input: none
// Output: 12-bit result of ADC conversions
// Samples ADC port designated in ADC_InitSWTrigger()
// 125k max sampling
// software trigger, busy-wait sampling
// Inputs: None.
// Output: Value sampled by ADC
uint16_t ADC_In(void);


// *************** ADC_InitSWTrigger ********************
// This initialization function sets up the ADC according to the
// following parameters. 
// Max sample rate: <=125,000 samples/second
// Sequencer 0 priority: 1st (lowest)
// Sequencer 1 priority: 2nd
// Sequencer 2 priority: 3rd
// Sequencer 3 priority: 4th (highest)
// SS2 triggering event: software trigger
// SS2 sample source: Ain0-11
// SS2 interrupts: enabled but not promoted to controller

// Inputs: channelNum - ADC channel input [0:11].
// Output: none
void ADC_InitSWTrigger(uint8_t channelNum);


// *************** channelInit ********************
// Initializes which ADC channel will be used.
// Inputs: channelNum - ADC channel input [0:11].
// Output: none
void channelInit(uint8_t channelNum);
