// Motors.c
// Runs on TM4C123 
// Contains all functions for low-level motor and servo control on autonomous
//  cars
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// 2016.04.20

///////////////////////////////////////////////////////////////////////////////
// motorInit()
// Initializes PB4,PB5,PB6,PB7 at motFrq frequency and 0 duty cycle.
//  Initializes PD0,PD1 at srvFrq frequency and 1.5ms duty cycle.
// Takes no input.
// Gives no output.

void motorInit(void);

///////////////////////////////////////////////////////////////////////////////
// motorUpdate()
// Converts two motor speeds to four PWM duty values, converts one servo
//  position into one PWM duty cycle, and updates six corresponding registries.
// Takes input of two int values representing two motor speeds and one servo
//  position.
// Gives no output.

void motorUpdate(int speed, int turn);
