// IR.h
// Runs on TM4C123
// Background thread using Timer1A to execute the function that getting the
// data from pre-initialized ADC, to implement a SW trigger ADC
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 8th, 2016


//********************Constants*********************************************
#define IR_1            0				//channel 0 PE3
#define IR_2						1				//channel 1 PE2
#define IR_FREQUENCY		1000		//for FFT
//**************************************************************************


//********************Externs***********************************************
extern uint8_t IR_IRToDisplay;          // determines which ping sensor's data to display in PingDisplay()
extern long x1[64],y1[64];            // input and output arrays for FFT
extern long x2[64],y2[64];            // input and output arrays for FFT
//**************************************************************************


//********************Prototypes********************************************

//******** IR_Init_HW *************** 
void IR_Init_HW(void);


//******** IRDisplay *************** 
// Foreground thread.
// Display IR sensor readings 10 times a second
// on the ST7735 as distance vs. time.
// inputs:  none
// outputs: none
void IRDisplay(void);

//******** IR_Init_SW *************** 
void IR_Init_SW(void);


//******** IRAccuracy *************** 
// Foreground thread.
// Initalizes sensor, collects data, and displays results. 
// inputs:  none
// outputs: none
void IRAccuracy(void);
