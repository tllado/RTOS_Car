// Lab7_Sensor.h
// Runs on TM4C123
// Real Time Operating System for Lab 7 Sensor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 11th, 2016


//**********************Compilter Directives********************************
  
// *************************************************************************

//********************Constants*********************************************
#define ADC_SAMPLE_RATE 400          // producer/consumer sampling rate
#define PWM_FREQ        40000        // PWM frequency. 40000 = 500Hz
//**************************************************************************


// ************* Enums ****************************************************
typedef enum{
	ST_STRAIGHT,
	ST_TURN_RIGHT,
	ST_TURN_LEFT,
	ST_SLIDE_RIGHT,
	ST_SLIDE_LEFT,
	ST_OBSTACLE_RIGHT,
	ST_OBSTACLE_LEFT,
	ST_REVERSE_RIGHT,
	ST_REVERSE_LEFT,
}StateType;
// *************************************************************************


//********************Externs***********************************************
extern uint8_t PingDebounce;               // only add one thread when button is pressed
extern uint8_t FirstRun;               // boolean that only adds race threads once when button is pressed. 
//**************************************************************************


//********************Prototypes********************************************

//**************************************************************************


