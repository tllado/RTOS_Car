// *********Can0.h ***************
// Runs on LM4F120/TM4C123
// Use CAN0 to communicate on CAN bus
// CAN0Rx PE4 (8) I TTL CAN module 0 receive.
// CAN0Tx PE5 (8) O TTL CAN module 0 transmit.

// Jonathan Valvano
// May 2, 2015

/* This example accompanies the books
   Embedded Systems: Real-Time Operating Systems for ARM Cortex-M Microcontrollers, Volume 3,  
   ISBN: 978-1466468863, Jonathan Valvano, copyright (c) 2015

   Embedded Systems: Real Time Interfacing to ARM Cortex M Microcontrollers, Volume 2
   ISBN: 978-1463590154, Jonathan Valvano, copyright (c) 2015

 Copyright 2014 by Jonathan W. Valvano, valvano@mail.utexas.edu
    You may use, edit, run or distribute this file
    as long as the above copyright notice remains
 THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
 OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
 VALVANO SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL,
 OR CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
 For more information about my classes, my research, and my books, see
 http://users.ece.utexas.edu/~valvano/
 */

#ifndef __CAN0_H__
#define __CAN0_H__
#define CAN_BITRATE             1000000

// reverse these IDs on the other microcontroller
#define RCV_ID 4
#define XMT_ID 2



extern uint8_t XmtData[8];           // data sent over CAN
extern uint8_t RcvData[8];           // data received over CAN
extern unsigned long PacketLost;     // packets lost
extern volatile uint8_t *CAN0PutPt;
extern volatile uint8_t *CAN0GetPt;

// Returns true if receive data is available
//         false if no receive data ready
int CAN0_CheckMail(void);

// if receive data is ready, gets the data and returns true
// if no receive data is ready, returns false
int CAN0_GetMailNonBlock(uint8_t data[8]);

// if receive data is ready, gets the data 
// if no receive data is ready, it waits until it is ready
void CAN0_GetMail(uint8_t data[8]);

// Initialize CAN port
void CAN0_Open(void);

// send 8 bytes of data to other microcontroller 
void CAN0_SendData(uint8_t data[8]);

// ******** CAN0_Fifo_Init ************
// Initialize the Fifo to be empty
// Inputs: size
// Outputs: none
void CAN0_Fifo_Init(unsigned long size);

// ******** CAN0_Fifo_Put ************
// Enter one data sample into the Fifo
// Called from the background, so no waiting.
// This function cannot enable/disable intterupts 
// since it will be called from interrupts.
// Inputs:  data
// Outputs: true - when data is properly saved,
//          false - if data not saved, because fifo was full
int CAN0_Fifo_Put(uint8_t data);


// ******** CAN0_Fifo_Get ************
// Remove one data sample from the Fifo
// Called in foreground, will block if empty
// Inputs:  none
// Outputs: data 
uint8_t CAN0_Fifo_Get(void);



#endif //  __CAN0_H__

