// Controller.c
// Runs on TM4C123
// Feedback controller for robot's motor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 23th, 2016

#include <stdint.h>
#include <stdbool.h>
#include "Controller.h"
#include "Lab7_Motor.h"
#include "Motors.h"
#include "Profiler.h"

void simpleController(void) {
    int speed = speedDefault;
    int direction = (int)(IR0_Data - IR3_Data)*KpSide/100 + \
                    (int)(IR1_Data - IR2_Data)*KpFront/100;

    speed = speedDefault;

    motorUpdate(speed, direction);
}
