// PWM.c
// Runs on TM4C123 
// Initiates and updates PWM signals on PB4, PB5, PB6, PB7, PD0, and PD1 pins
//  for motor and servo control
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 19th, 2016

///////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include "tm4c123gh6pm.h"

///////////////////////////////////////////////////////////////////////////////
// User Settings

const uint16_t motFrq = 800;        // Hz
const uint16_t srvFrq = 50;         // Hz
const uint32_t sysFrq = 80000000;   // Hz
const uint16_t PWMDiv = 32;         // System Clock ticks per PWM Clock tick

///////////////////////////////////////////////////////////////////////////////
// Global Variables

const uint16_t motPrdDef = sysFrq/PWMDiv/motFrq;
const uint16_t motDtyDef = 0;                   // idle
const uint16_t srvPrdDef = sysFrq/PWMDiv/srvFrq;
const uint16_t srvDtyDef = srvPrdDef * 3/2/20;  // 1.5ms, center

////////////////////////////////////////////////////////////////////////////////
// Motor Speed Lookup Tables
// These empirically derived lookup tables convert 8-bit motor speed values
// (1=full reverse, 128 = idle, 255 = full forward) to PWM duty periods,
// assuming a PWM frequency of 800Hz and a CPU/PWM divider of 32/1 (therefore a
// full duty of 3125).

uint16_t LeftPWM[255] = {
       0, 2925, 2900, 2877, 2853, 2828, 2805, 2781, 2758, 2734, 2713, 2689, 
    2666, 2644, 2622, 2600, 2578, 2556, 2534, 2513, 2491, 2469, 2447, 2427, 
    2405, 2384, 2363, 2341, 2320, 2298, 2278, 2256, 2234, 2214, 2192, 2172, 
    2150, 2128, 2108, 2086, 2066, 2044, 2022, 2000, 1978, 1956, 1934, 1914, 
    1892, 1870, 1848, 1827, 1805, 1783, 1761, 1738, 1716, 1694, 1672, 1650, 
    1628, 1606, 1583, 1561, 1538, 1516, 1494, 1472, 1450, 1427, 1405, 1381, 
    1359, 1338, 1316, 1294, 1272, 1248, 1227, 1205, 1183, 1161, 1139, 1117, 
    1095, 1073, 1052, 1031, 1009,  988,  966,  944,  923,  902,  881,  859, 
     838,  817,  797,  775,  755,  733,  713,  691,  669,  648,  628,  606, 
     584,  563,  541,  519,  497,  475,  453,  430,  406,  381,  358,  333, 
     306,  281,  253,  222,  191,  156,  119,   70,    0,   53,   97,  134, 
     169,  200,  228,  259,  286,  313,  338,  366,  391,  414,  439,  463, 
     488,  509,  534,  558,  581,  603,  628,  650,  673,  697,  719,  744, 
     766,  789,  813,  836,  859,  881,  906,  928,  953,  975, 1000, 1023, 
    1047, 1070, 1094, 1119, 1142, 1166, 1191, 1213, 1238, 1263, 1284, 1309, 
    1334, 1358, 1381, 1406, 1428, 1453, 1478, 1500, 1525, 1548, 1572, 1597, 
    1619, 1644, 1666, 1691, 1713, 1738, 1759, 1784, 1806, 1830, 1853, 1875, 
    1900, 1922, 1944, 1969, 1991, 2013, 2036, 2059, 2081, 2105, 2128, 2150, 
    2173, 2197, 2219, 2241, 2264, 2288, 2309, 2334, 2356, 2380, 2403, 2425, 
    2450, 2472, 2497, 2520, 2544, 2569, 2592, 2616, 2641, 2666, 2689, 2714, 
    2739, 2764, 2789, 2816, 2841, 2866, 2891, 2917, 2944, 2969, 2995, 3022, 
    3047, 3073, 3100, 3125};

uint16_t RightPWM[255] = {
       0, 3125, 3094, 3063, 3031, 3003, 2972, 2944, 2916, 2889, 2863, 2834, 
    2809, 2784, 2758, 2733, 2708, 2684, 2659, 2634, 2611, 2588, 2563, 2541, 
    2517, 2494, 2472, 2448, 2425, 2403, 2380, 2358, 2334, 2313, 2291, 2269, 
    2245, 2223, 2200, 2178, 2156, 2134, 2113, 2091, 2067, 2045, 2022, 2000, 
    1978, 1956, 1933, 1909, 1888, 1866, 1842, 1819, 1797, 1773, 1750, 1728, 
    1705, 1681, 1659, 1634, 1613, 1589, 1566, 1542, 1519, 1495, 1472, 1448, 
    1425, 1402, 1378, 1356, 1331, 1309, 1284, 1263, 1238, 1216, 1192, 1169, 
    1145, 1122, 1100, 1077, 1053, 1031, 1008,  984,  963,  941,  917,  894, 
     872,  850,  828,  805,  783,  759,  738,  716,  694,  672,  648,  625, 
     603,  581,  558,  534,  513,  488,  466,  441,  416,  391,  366,  341, 
     313,  286,  256,  228,  194,  159,  119,   70,    0,   69,  116,  156, 
     194,  225,  256,  288,  316,  344,  369,  397,  422,  447,  472,  497, 
     519,  544,  567,  591,  614,  638,  661,  684,  706,  730,  753,  775, 
     798,  822,  844,  867,  891,  913,  936,  959,  981, 1006, 1028, 1052, 
    1075, 1098, 1122, 1144, 1169, 1191, 1216, 1238, 1263, 1284, 1309, 1333, 
    1356, 1380, 1403, 1427, 1450, 1473, 1497, 1520, 1544, 1567, 1591, 1614, 
    1638, 1661, 1684, 1708, 1731, 1755, 1778, 1800, 1825, 1847, 1870, 1894, 
    1916, 1939, 1963, 1984, 2008, 2031, 2053, 2078, 2100, 2122, 2147, 2169, 
    2192, 2216, 2238, 2261, 2284, 2306, 2331, 2353, 2377, 2400, 2423, 2447, 
    2470, 2494, 2517, 2541, 2566, 2589, 2613, 2638, 2661, 2684, 2709, 2734, 
    2758, 2783, 2806, 2831, 2856, 2881, 2906, 2931, 2955, 2980, 3003, 3028, 
    3053, 3077, 3100, 3125};

///////////////////////////////////////////////////////////////////////////////
// motorInit()
// Initializes PB4,PB5,PB6,PB7 at motFrq frequency and 0 duty cycle.
//  Initializes PD0,PD1 at srvFrq frequency and 1.5ms duty cycle.
// Takes no input.
// Gives no output.

void motorInit(void) {
    // Initialize PB4, PB5, PB6, PB7, PD0, and PD1 for PWM
    SYSCTL_RCGCPWM_R |= SYSCTL_RCGCPWM_R0;  // activate clock for PWM Mod 0
    SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R1|SYSCTL_RCGCGPIO_R3;
                                            // activate clock for Port B
    while((SYSCTL_PRGPIO_R&SYSCTL_PRGPIO_R1) == 0){};
                                            // allow time to finish activating
    while((SYSCTL_PRGPIO_R&SYSCTL_PRGPIO_R3) == 0){};
                                            // allow time to finish activating
    GPIO_PORTB_AFSEL_R |= 0xF0;             // enable alt funct on PB4-7
    GPIO_PORTB_PCTL_R &= ~0xFFFF0000;
    GPIO_PORTB_PCTL_R |= 0x44440000;        // configure PB4-7 as PWM
    GPIO_PORTB_AMSEL_R &= ~0xF0;            // disable analog function on PB4-7
    GPIO_PORTB_DEN_R |= 0xF0;               // enable digital I/O on PB4-7
    GPIO_PORTD_AFSEL_R |= 0x03;             // enable alt funct on PD0-1
    GPIO_PORTD_PCTL_R &= ~0x000000FF;
    GPIO_PORTD_PCTL_R |= 0x00000044;        // configure PD0-1 as PWM
    GPIO_PORTD_AMSEL_R &= ~0x03;            // disable analog function on PD0-1
    GPIO_PORTD_DEN_R |= 0x03;               // enable digital I/O on PD0-1
        
    // Set PWM clock to fractin of system clock
    SYSCTL_RCC_R |= SYSCTL_RCC_USEPWMDIV;
    SYSCTL_RCC_R &= ~SYSCTL_RCC_PWMDIV_M;
    SYSCTL_RCC_R += SYSCTL_RCC_PWMDIV_32;
        
    // Setup PB4-7, PD0-1 for motor control
    PWM0_0_CTL_R = 0;                       // set countdown mode for Mod0 Blk0
    PWM0_1_CTL_R = 0;                       // set countdown mode for Mod0 Blk1
    PWM0_3_CTL_R = 0;                       // set countdown mode for Mod0 Blk3
    PWM0_0_GENA_R = (PWM_0_GENA_ACTCMPAD_ONE|PWM_0_GENA_ACTLOAD_ZERO);
    PWM0_0_GENB_R = (PWM_0_GENB_ACTCMPBD_ONE|PWM_0_GENB_ACTLOAD_ZERO);
    PWM0_1_GENA_R = (PWM_1_GENA_ACTCMPAD_ONE|PWM_1_GENA_ACTLOAD_ZERO);
    PWM0_1_GENB_R = (PWM_1_GENB_ACTCMPBD_ONE|PWM_1_GENB_ACTLOAD_ZERO);
    PWM0_3_GENA_R = (PWM_3_GENA_ACTCMPAD_ONE|PWM_3_GENA_ACTLOAD_ZERO);
    PWM0_3_GENB_R = (PWM_3_GENB_ACTCMPBD_ONE|PWM_3_GENB_ACTLOAD_ZERO);
                                            // define signal triggers
    PWM0_0_LOAD_R = motPrdDef;
    PWM0_1_LOAD_R = motPrdDef;
    PWM0_3_LOAD_R = srvPrdDef;              // set counter reset value (period)
    PWM0_0_CMPA_R = motDtyDef;              // set comparator value (duty)
    PWM0_0_CMPB_R = motDtyDef;
    PWM0_1_CMPA_R = motDtyDef;
    PWM0_1_CMPB_R = motDtyDef;
    PWM0_3_CMPA_R = srvDtyDef;
    PWM0_3_CMPB_R = srvDtyDef;
    PWM0_0_CTL_R |= PWM_0_CTL_ENABLE;       // start Mod 0 Blk 0
    PWM0_1_CTL_R |= PWM_1_CTL_ENABLE;       // start Mod 0 Blk 1
    PWM0_3_CTL_R |= PWM_3_CTL_ENABLE;       // start Mod 0 Blk 3
    PWM0_ENABLE_R |= (PWM_ENABLE_PWM0EN|PWM_ENABLE_PWM1EN|PWM_ENABLE_PWM2EN|\
    PWM_ENABLE_PWM3EN|PWM_ENABLE_PWM6EN|PWM_ENABLE_PWM7EN);
                                            // enable PWM Module 0
}

///////////////////////////////////////////////////////////////////////////////
// updateDuties()
// Converts two motor speeds to four PWM duty values, converts one servo
//  position into one PWM duty cycle, and updates six corresponding registries.
// Takes input of three uint8_t values representing two motor speeds and one
//  servo position.
// Gives no output.

void updateDuties(uint8_t speedL, uint8_t speedR, uint16_t servoPos){
    // Set left motor speed
    if(speedL > 128) {
        PWM0_0_CMPA_R = LeftPWM[speedL];
        PWM0_0_CMPB_R = 0;
    }
    else {
        PWM0_0_CMPA_R = 0;
        PWM0_0_CMPB_R = LeftPWM[speedL];
    }
    
    // Set right motor speed
    if(speedR > 128) {
        PWM0_1_CMPA_R = 0;
        PWM0_1_CMPB_R = RightPWM[speedR];
    }
    else {
        PWM0_1_CMPA_R = RightPWM[speedR];
        PWM0_1_CMPB_R = 0;
    }

    // Set servo positions
    PWM0_3_CMPA_R = 5000 + servoPos*19;
    PWM0_3_CMPB_R = 0;  // Currently not in use
}
