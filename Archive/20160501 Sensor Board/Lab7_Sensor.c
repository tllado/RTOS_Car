// Lab7_Sensor.c
// Runs on TM4C123
// Real Time Operating System for Lab 7 Sensor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// 2016.04.30

////////////////////////////////////////////////////////////////////////////////
// ST7735 Interface

// Backlight (pin 10) connected to +3.3 V
// MISO (pin 9) connected to PA4 (SSI0Rx)
// SCK (pin 8) connected to PA2 (SSI0Clk)
// MOSI (pin 7) connected to PA5 (SSI0Tx)
// TFT_CS (pin 6) connected to PA3 (SSI0Fss) <- GPIO high to disable TFT
// CARD_CS (pin 5) connected to PB0 GPIO output 
// Data/Command (pin 4) connected to PA6 (GPIO)<- GPIO low not using TFT
// RESET (pin 3) connected to PA7 (GPIO)<- GPIO high to disable TFT
// VCC (pin 2) connected to +3.3 V
// Gnd (pin 1) connected to ground

////////////////////////////////////////////////////////////////////////////////
// Timers

//  SysTick - OS_Launch() ; priority = 7
//  Timer0A - ADC_InitHWTrigger() ; priority = 2
//  Timer1A - Ping1() ; priority = 2
//  Timer4A - OS_AddPeriodicThread(), Os_AddAperiodicThread() ; priority = 1
//  Timer5A - OS_Init(), OS_Time(), OS_TimeDifference(), OS_ClearMsTime(),
//            OS_MsTime() ; priority = 0

////////////////////////////////////////////////////////////////////////////////
// OS Background Threads

//  PingCompute - priority = 2
//  CANProducer - priority = 3
//  OS_Aging - priority = 7 (only added if PRIORITY_SCHEDULER is defined in OS.h)
//  OS_Sleep - priority = 1

////////////////////////////////////////////////////////////////////////////////
// Dependencies

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "ADC.h"
#include "Buttons.h"
#include "CAN_4C123\can0.h"
#include "Interrupts.h"
#include "IR.h"
#include "Lab7_Sensor.h"
#include "OS.h"
#include "Profiler.h"
#include "Retarget.h"
#include "SysTick.h"
#include "Timer5.h"
#include "tm4c123gh6pm.h"
#ifdef DEBUG
    #include "Interpreter.h"
    #include "ST7735.h"
    #include "UART.h"
#endif

////////////////////////////////////////////////////////////////////////////////
// Global Variables

uint32_t IdleCount; // counts iterations of idleTask()
uint8_t FirstRun;   // indicates whether race has started
uint8_t StartFrame[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF};
                    // start frame sent from sensor board to motor board

////////////////////////////////////////////////////////////////////////////////
// idleTask()
// Foreground thread. 
// Runs when no other work is needed.
// Never blocks, sleeps, or dies.
// inputs:  none
// outputs: none

void IdleTask(void){ 
    while(1) { 
        PF1 ^= 0x02;        // debugging profiler 
        IdleCount++;        // debugging 
        WaitForInterrupt(); // low-power mode
    }
}

////////////////////////////////////////////////////////////////////////////////
// interpreter()
// Foreground thread.
// Accepts input from serial port, outputs to serial port.
// inputs:  none
// outputs: none

#ifdef DEBUG
    void Interpreter(void){   
        Interpreter_Init();
        while(1){
            Interpreter_Parse();
        }
    }
#endif

////////////////////////////////////////////////////////////////////////////////
// CANProducer()
// Periodic background thread.
// Sends sensor data over CAN.
// inputs:  none
// outputs: none

const uint8_t numFrames = 8;
void CANProducer(void){ 
    uint8_t frame[numFrames];
    frame[0] = IR0_Dist;
    frame[1] = IR1_Dist;
    frame[2] = IR2_Dist;
    frame[3] = IR3_Dist;
    frame[4] = 0;
    frame[5] = 0;
    frame[6] = 0;

//    if(Bumper0_Data){
//        Bumper0_Data = false;
//        GPIO_PORTC_IM_R |= 0x40;    // (g) arm interrupt on PC6
//    }
//    if(Bumper1_Data){
//        Bumper1_Data = false;
//        GPIO_PORTC_IM_R |= 0x80;    // (g) arm interrupt on PC7
//    }
    
    CAN0_SendData(frame);           // send 64 bits of sensor data  
}

////////////////////////////////////////////////////////////////////////////////
// ButtonWorkRace()
// Aperiodic Background thread.
// Begins race when pressed.
// Can only add threads once.
// inputs:  none
// outputs: none

void ButtonWorkRace(void){
    if(FirstRun){
        FirstRun = 0;

        // add threads needed for race
        OS_AddPeriodicThread(&CANProducer, FIFTY_HZ, 1);
//        OS_AddButtonTask(&Bumper0Task, PC6_TASK, 2);
//        OS_AddButtonTask(&Bumper1Task, PC7_TASK, 2);

        // send start signal to motor board
        CAN0_SendData(StartFrame);
        Timer5A_Enable();   // Stop robot after 3 minutes
    }
}

int main(void){  
    // intialize globals
    IdleCount = 0;  // number of times IdleTask() is ran
    FirstRun = 1;   // prevents ButtonWorkRace() from running more than once
                
    // initalize modules  
    OS_Init();      // initialize OS, disable interrupts
    PortF_Init();   // initialize Port F profiling
    CAN0_Open();    // initialize CAN0
    IR_Init_HW();   // initialize IR sensors 0-3

    // add button tasks
    OS_AddButtonTask(&ButtonWorkRace, PF4_TASK, 3);
    
    // add initial foreground threads
    OS_AddThread(&IdleTask, 128, 7);
    #ifdef DEBUG
        OS_AddThread(&Interpreter, 128, 3);
        OS_AddThread(&IRDisplay, 128, 4);
    #endif

    // finished initialization
    OS_Launch(TIMESLICE);   // doesn't return, interrupts enabled in here
    return 0;               // this never executes
}
