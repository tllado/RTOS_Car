// Buttons.c
// Runs on TM4C123
// Request an interrupt on the falling edge of PF0 and/or PF4 (when the 
// user buttons are pressed) Note that button bouncing is not addressed.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// April 11th, 2016


#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "stdbool.h"
#include "Buttons.h"
#include "Interrupts.h"
#include "OS.h"
#include "Ping.h"


// ***************Globals**********************
void(*SW1Task)(void);     // user function on falling edge of SW1(PF4)
void(*SW2Task)(void);     // user function on falling edge of SW2(PF0)
void(*BUMPER0Task)(void); // user function on falling edge of BUMPER0(PC6)
void(*BUMPER1Task)(void); // user function on falling edge of BUMPER1(PC7)
uint8_t SW1Priority;      // priority of SW1's user task
uint8_t SW2Priority;      // priority of SW2's user task
uint8_t BUMPER0Priority;  // priority of BUMPER0's user task
uint8_t BUMPER1Priority;  // priority of BUMPER1's user task

volatile uint8_t Bumper0_Data;    // Bumper0 data
volatile uint8_t Bumper1_Data;    // Bumper1 data
// ********************************************


// ******** SW1_Init ************
// Initialize Port F4 to handle edge triggered interrupt when SW1 is pressed. 
// Store SW1's task and priority.
// input:  *task - pointer to SW1's user task
//         priority - SW1's task priority
// output: none
void SW1_Init(void(*task)(void), uint8_t priority){ 
	int32_t status = StartCritical();
	SW1Task = task;               // save user task for SW1
	SW1Priority = priority;       // save priority for SW1
  SYSCTL_RCGCGPIO_R |= 0x00000020; // (a) activate clock for port F   
	while((SYSCTL_PRGPIO_R&0x20) == 0){}; // allow time for clock to stabilize
  GPIO_PORTF_DIR_R &= ~0x10;    // (c) make PF4 in (built-in button)
  GPIO_PORTF_AFSEL_R &= ~0x10;  //     disable alt funct on PF4
  GPIO_PORTF_DEN_R |= 0x10;     //     enable digital I/O on PF4   
  GPIO_PORTF_PCTL_R &= ~0x000F0000; // configure PF4 as GPIO
  GPIO_PORTF_AMSEL_R &= ~0x10;  //     disable analog functionality on PF4
  GPIO_PORTF_PUR_R |= 0x10;     //     enable weak pull-up on PF4
  GPIO_PORTF_IS_R &= ~0x10;     // (d) PF4 is edge-sensitive
  GPIO_PORTF_IBE_R &= ~0x10;    //     PF4 is not both edges
  GPIO_PORTF_IEV_R &= ~0x10;    //     PF4 falling edge event
  GPIO_PORTF_ICR_R = 0x10;      // (e) clear flag4
  GPIO_PORTF_IM_R |= 0x10;      // (f) arm interrupt on PF4 
  NVIC_PRI7_R &= 0xFF00FFFF;    //     priority 0
	NVIC_PRI7_R |= 0x00000000;    //     priority 0
  NVIC_EN0_R = 0x40000000;      // (h) enable interrupt 30 in NVIC
	EndCritical(status);
}


// ******** SW2_Init ************
// Initialize Port F0 to handle edge triggered interrupt when SW2 is pressed. 
// Store SW2's task and priority.
// input:  *task - pointer to SW2's user task
//         priority - SW2's task priority
// output: none
void SW2_Init(void(*task)(void), uint8_t priority){ 
	int32_t status = StartCritical();
	SW2Task = task;               // save user task for SW2
	SW2Priority = priority;       // save priority for SW2
  SYSCTL_RCGCGPIO_R |= 0x00000020; // (a) activate clock for port F   
	while((SYSCTL_PRGPIO_R&0x20) == 0){}; // allow time for clock to stabilize
  GPIO_PORTF_LOCK_R = GPIO_LOCK_KEY;    // b1) unlock GPIO Port F Commit Register
  GPIO_PORTF_CR_R |= (SW1|SW2);         // b2) enable commit for PF4 and PF0
  GPIO_PORTF_DIR_R &= ~0x01;    // (d) make PF0 in (built-in button)
  GPIO_PORTF_AFSEL_R &= ~0x01;  //     disable alt funct on PF0
  GPIO_PORTF_DEN_R |= 0x01;     //     enable digital I/O on PF0   
  GPIO_PORTF_PCTL_R &= ~0x0000000F; // configure PF0 as GPIO
  GPIO_PORTF_AMSEL_R &= ~0x01;  //     disable analog functionality on PF0
  GPIO_PORTF_PUR_R |= 0x01;     //     enable weak pull-up on PF0
  GPIO_PORTF_IS_R &= ~0x01;     // (e) PF0 is edge-sensitive
  GPIO_PORTF_IBE_R &= ~0x01;    //     PF0 is not both edges
  GPIO_PORTF_IEV_R &= ~0x01;    //     PF0 falling edge event
  GPIO_PORTF_ICR_R = 0x01;      // (f) clear flag0
  GPIO_PORTF_IM_R |= 0x01;      // (g) arm interrupt on PF0 
  NVIC_PRI7_R &= 0xFF00FFFF;    //     priority 0
	NVIC_PRI7_R |= 0x00000000;    //     priority 0
  NVIC_EN0_R = 0x40000000;      // (h) enable interrupt 30 in NVIC
	EndCritical(status);
}

// ******** BUMPER0_Init ************
// Initialize PC6 to handle edge triggered interrupt with BUMPER0. 
// Store BUMPER0's task and priority.
// input:  *task - pointer to BUMPER0's user task
//         priority - BUMPER0's task priority
// output: none
void BUMPER0_Init(void(*task)(void), uint8_t priority){ 
	int32_t status = StartCritical();
	BUMPER0Task = task;               // save user task for BUMPER0
	BUMPER0Priority = priority;       // save priority for BUMPER0
  SYSCTL_RCGCGPIO_R |= 0x00000004; // (a) activate clock for port C   
	while((SYSCTL_PRGPIO_R&0x04) == 0){}; // allow time for clock to stabilize
  GPIO_PORTC_DIR_R &= ~0x40;    // (d) make PC6 in (built-in button)
  GPIO_PORTC_AFSEL_R &= ~0x40;  //     disable alt funct on PC6
  GPIO_PORTC_DEN_R |= 0x40;     //     enable digital I/O on PC6   
  GPIO_PORTC_PCTL_R &= ~0x0F000000; // configure PC6 as GPIO
  GPIO_PORTC_AMSEL_R &= ~0x40;  //     disable analog functionality on PC6
  GPIO_PORTC_IS_R &= ~0x40;     // (e) PC6 is edge-sensitive
  GPIO_PORTC_IBE_R &= ~0x40;    //     PC6 is not both edges
  GPIO_PORTC_IEV_R |= 0x40;     //     PC6 rising edge event
  GPIO_PORTC_ICR_R = 0x40;      // (f) clear flag6
  GPIO_PORTC_IM_R |= 0x40;      // (g) arm interrupt on PC6
  NVIC_PRI0_R &= 0xFF00FFFF;    //     priority 0
	NVIC_PRI0_R |= 0x00000000;    //     priority 0
  NVIC_EN0_R = 0x00000004;      // enable interrupt 2
	EndCritical(status);
}


// ******** BUMPER1_Init ************
// Initialize PC7 to handle edge triggered interrupt with BUMPER1. 
// Store BUMPER1's task and priority.
// input:  *task - pointer to BUMPER1's user task
//         priority - BUMPER1's task priority
// output: none
void BUMPER1_Init(void(*task)(void), uint8_t priority){ 
	int32_t status = StartCritical();
	BUMPER1Task = task;               // save user task for BUMPER1
	BUMPER1Priority = priority;       // save priority for BUMPER1
  SYSCTL_RCGCGPIO_R |= 0x00000004; // (a) activate clock for port C   
	while((SYSCTL_PRGPIO_R&0x04) == 0){}; // allow time for clock to stabilize
  GPIO_PORTC_DIR_R &= ~0x80;    // (d) make PC7 in (built-in button)
  GPIO_PORTC_AFSEL_R &= ~0x80;  //     disable alt funct on PC7
  GPIO_PORTC_DEN_R |= 0x80;     //     enable digital I/O on PC7   
  GPIO_PORTC_PCTL_R &= ~0xF0000000; // configure PC7 as GPIO
  GPIO_PORTC_AMSEL_R &= ~0x80;  //     disable analog functionality on PC7
  GPIO_PORTC_IS_R &= ~0x80;     // (e) PC7 is edge-sensitive
  GPIO_PORTC_IBE_R &= ~0x80;    //     PC7 is not both edges
  GPIO_PORTC_IEV_R |= 0x80;     //     PC7 rising edge event
  GPIO_PORTC_ICR_R = 0x80;      // (f) clear flag7
  GPIO_PORTC_IM_R |= 0x80;      // (g) arm interrupt on PC7 
  NVIC_PRI0_R &= 0xFF00FFFF;    //     priority 0
	NVIC_PRI0_R |= 0x00200000;    //     priority 0
  NVIC_EN0_R = 0x00000004;      // enable interrupt 2
	EndCritical(status);
}


// ***************** GPIOPortC_Handler ****************
// Adds an aperiodic background task to whenever the BUMPER0(PC6) or BUMPER1(PC7) 
// bumpers are pressed.  Also handles capturing Ping2's pulse width (See Ping.c).
void GPIOPortC_Handler(void){
	// BUMPER0 
	if(GPIO_PORTC_RIS_R & 0x40){                                     // poll PC6(BUMPER0)
    GPIO_PORTC_ICR_R = 0x40;                                       // acknowledge flag6
    OS_AddAperiodicThread(BUMPER0Task, BUMPER0Priority); 
	}
	
	// BUMPER1
	if(GPIO_PORTC_RIS_R & 0x80){                                     // poll PC7(BUMPER1)
    GPIO_PORTC_ICR_R = 0x80;                                       // acknowledge flag7
    OS_AddAperiodicThread(BUMPER1Task, BUMPER1Priority);
	}
}


// ***************** GPIOPortF_Handler ******************
// Adds an aperiodic background task to whenever the SW1(PF4) or SW2(PF0) buttons are pressed
void GPIOPortF_Handler(void){
	if(GPIO_PORTF_RIS_R & 0x01){  // poll PF0(SW2)
    GPIO_PORTF_ICR_R = 0x01;    // acknowledge flag0
    OS_AddAperiodicThread(SW2Task, SW2Priority); 
	}
	if(GPIO_PORTF_RIS_R & 0x10){  // poll PF4(SW1)
    GPIO_PORTF_ICR_R = 0x10;    // acknowledge flag4
    OS_AddAperiodicThread(SW1Task, SW1Priority);
	}
}


// ***************** Switch_Enable ******************
void Switch_Enable(void){
	NVIC_EN0_R = 1<<30;                // enable IRQ 30 in NVIC
}


// ***************** Switch_Disable ******************
void Switch_Disable(void){
	NVIC_DIS0_R = 1<<30;               // disable IRQ 30 in NVIC
}


//******** Bumper0Task **************
// Aperiodic background thread.  
// Called when bumper0 is pressed, and sets state of
// robot into reverse left.
// inputs:  none
// outputs: none
void Bumper0Task(void){
	GPIO_PORTC_IM_R &= ~0x40;      // (g) disarm interrupt on PC6
	GPIO_PORTC_ICR_R = 0x40;       // (f) clear flag6
	Bumper0_Data = true;
}


//******** Bumper1Task **************
// Aperiodic background thread.  
// Called when bumper1 is pressed, and sets state of
// robot into reverse right.
// inputs:  none
// outputs: none
void Bumper1Task(void){
	GPIO_PORTC_IM_R &= ~0x80;      // (g) disarm interrupt on PC7
	GPIO_PORTC_ICR_R = 0x80;       // (f) clear flag7
	Bumper1_Data = true;
}
