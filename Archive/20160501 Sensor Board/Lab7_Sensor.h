// Lab7_Sensor.h
// Runs on TM4C123
// Real Time Operating System for Lab 7 Sensor board.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// 2016.04.30

////////////////////////////////////////////////////////////////////////////////
// Debug mode?

//#define DEBUG

////////////////////////////////////////////////////////////////////////////////
// Global Variables

#define FIFTY_HZ	20*80000		// 20ms*80000cycles/ms
extern uint8_t PingDebounce;  // only add one thread when button is pressed
extern uint8_t FirstRun;      // boolean that only adds race threads once when button is pressed. 
