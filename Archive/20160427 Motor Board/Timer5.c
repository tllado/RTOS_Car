// Timer5.c
// Runs on TM4C123 
// Use TIMER5 in 32-bit periodic mode.  Used for calculating time.  
// Does not trigger interrupts.
// Brandon Boesch
// Ari Levy
// Travis Llado
// Ce Wei
// February 21, 2016


#include <stdint.h>
#include "tm4c123gh6pm.h"
#include "Timer5.h"
#include "Interrupts.h"
#include "Profiler.h"
#include "Motors.h"

//******************** Globals ***********************************************
uint32_t GlobalClock = 0;                               // Current run time of the OS (30 seconds per increment)
//****************************************************************************


// ***************** Timer5_Init ****************
// Activate Timer5 to count time for tasks
// Inputs:  Period in units (1/clockfreq)
// Outputs: none
void Timer5_Init(unsigned long period){
  SYSCTL_RCGCTIMER_R |= 0x20;                           // 0) activate timer5
	while((SYSCTL_PRTIMER_R&0x20) == 0){};                // allow time for clock to stabilize
  TIMER5_CTL_R = 0x00000000;                            // 1) disable timer5A during setup
  TIMER5_CFG_R = 0x00000000;                            // 2) configure for 32-bit mode
  TIMER5_TAMR_R = 0x00000002;                           // 3) configure for periodic mode, default down-count settings
  TIMER5_TAILR_R = period-1;                            // 4) reload value
  TIMER5_TAPR_R = 0;                                    // 5) bus clock resolution
  TIMER5_ICR_R = 0x00000001;                            // 6) clear timer5A timeout flag
  TIMER5_IMR_R = 0x00000001;                            // 7) arm timeout interrupt
	NVIC_PRI23_R &=	0xFFFFFF00;                           // 8) priority 0
	NVIC_PRI23_R |=	0x00000000; 	                        // 8) priority 0
//  NVIC_EN2_R = 1<<(92-(32*2));                          // 9) enable IRQ 92 in NVIC
//  TIMER5_CTL_R = 0x00000001;                            // 10) enable timer5A
}

void Timer5A_Handler(void){
  TIMER5_ICR_R = TIMER_ICR_TATOCINT;                    // acknowledge TIMER5A timeout
	GlobalClock++;                                        // current runtime of OS, in 30 second increments
	if(GlobalClock == 6){                                 // if 180 seconds has passed, stall OS
		DisableInterrupts();
	  motorUpdate(0,0);
		PF2 = 0x04;                                         // Turn on Blue LED to signify that 3 minutes is up 
		while(1){};
	}
}


// ***************** Timer5A_Enable ******************
void Timer5A_Enable(void){
  NVIC_EN2_R = 1<<(92-(32*2));                          //  enable IRQ 92 in NVIC
  TIMER5_CTL_R = 0x00000001;                            //  enable timer5A
}


// ***************** Timer5A_Disable ******************
void Timer5A_Disable(void){
  NVIC_DIS2_R = 1<<(92-(32*2));                          //  enable IRQ 92 in NVIC
	TIMER5_CTL_R = 0x00000000;                             // disable TIMER5A
}
